const path = require('path');
const express = require('express');
const morgan = require('morgan');
const http = require('http');
const socketio = require('socket.io');

const { addUser, removeUser, getUser, getUsersInRoom } = require('./util/userChatEvent');

const app = express();
const port = 5000;


const server = http.createServer(app);
const io = socketio(server);

//socket io event
io.on('connection', function (socket) {
    socket.on('disconnect', function () {
        const user = removeUser(socket.id);

        if (user) {
            io.to(user.room).emit('message', { user: 'admin', text: `${user.name} has left.` });
        }
    })

    socket.on('join', function (data, callback) {
        const user = addUser({
            id: socket.id,
            name: data.name,
            room: data.room
        });
        console.log(user);


        socket.join(user.room);

        callback();
    });

    socket.on('sendMessage', (message, name, callback) => {
        const user = getUser(name);
        // console.log(user.room);
        io.to(user.room).emit('message', {
            user: user.name,
            text: message
        });

        callback();
    })
});

const route = require('./routes');
const db = require('./config/db');

//Connect to db
db.connect();

app.use(express.static(path.join(__dirname, 'public')));

//middleware xử lý data gửi lên bằng phương thức POST
app.use(express.urlencoded());
app.use(express.json());

// app.use(morgan('combined'));

// app.engine('hbs', handlebars({
//     extname: '.hbs'
// }));
// app.set('view engine', 'hbs');
// app.set('views', path.join(__dirname, 'resources', 'views'));

//routes list
route(app);

server.listen({ port, 'Access-Control-Allow-Origin': "*" }, () => {
    console.log(`App listening at http://localhost:${port}`)
});