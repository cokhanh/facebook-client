const chatRoutes = require('./chat');

function route(app) {
    app.use('/chat', chatRoutes);
}

module.exports = route;