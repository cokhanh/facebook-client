const { check } = require('express-validator');

const ChatValidator = () => {
    return [
        check('id', 'id is required').not().isEmpty()
    ];
}

module.exports = { ChatValidator };