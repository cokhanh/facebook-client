const Chat = require('../models/Chat');
const { multipleMongooseToObject } = require('../../util/mongoose');
const Controller = require('./Controller');

class ChatController {
    //[POST] /chat/create
    createChat(req, res, next) {
        const formData = req.body;
        const chat = new Chat(formData);
        chat.save();

        res.send('OK');
    }
    index(req,res,next){
        res.send('hello');
    }
}

module.exports = new ChatController;