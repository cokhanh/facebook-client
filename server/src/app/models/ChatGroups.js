const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ChatGroups = new Schema({
    user_ids: { type: Array, required: true },
    room_name: { type: String, required: true }
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

module.exports = mongoose.model('Chat_groups', ChatGroups);