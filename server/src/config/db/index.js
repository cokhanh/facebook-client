const mongoose = require('mongoose');

async function connect() {
    try {
        await mongoose.connect('mongodb+srv://khanh-khai-api:khanh-khai-api@tlcn-db-server.l4b9p.mongodb.net/tlcn-chat-db', {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        console.log('connect successfully!!');
    } catch (err) {
        console.log('connect failed!!');
    }
}

module.exports = { connect };