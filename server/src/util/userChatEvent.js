const users = [];

const addUser = ({ id, name, room }) => {
    //co khanh = cokhanh
    console.log(name, room);
    if (name && room) {
        name = name.trim().toLowerCase();
        room = room.trim().toLowerCase();

        const existingUser = users.find((user) => user.name === name && user.room === room);

        const user = { id, name, room };
        // console.log(user);
        users.push(user);
        return user;

    }

}

const removeUser = (id) => {
    const index = users.findIndex((user) => user.id === id);
    if (index !== -1) {
        return users.splice(index, 1)[0];
    }
}

const getUser = (name) => users.find((user) => user.name === name);

const getUsersInRoom = (room) => users.filter((user) => user.room === room);

module.exports = { addUser, removeUser, getUser, getUsersInRoom };