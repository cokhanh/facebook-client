import React, { Component } from "react";
import PublicImage from "../HinhAnh/Public.png";
import FriendIcon from "../HinhAnh/FriendIcon.png";
import PrivateICon from "../HinhAnh/PrivateICon.png";
import TextareaAutosize from 'react-textarea-autosize';
import fbPictureAdd from "../HinhAnh/fbPictureAdd.png"
import tagName from "../HinhAnh/tagName.png"
import marker from "../HinhAnh/marker.png"
import API from "../API/API.js";
import './UpStatus.css'

class StatusContent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            src: PublicImage,
            alt: "PublicIcon",
            name: "Công khai",
            class: "PublicImageIcon",
            statusSetting: "pub",
            ContentWhoAndWhere: "Với",
            WithWhoOrWhere: "Cùng với ai?",
            tagsName: false,
            checkin: false,
            idTagsName: "txtTagsName",
            VisibleIMGdk: false,
            widthProgress: 0,
            arraySearch: [],
            arrayImage: [],
        };
    }
    setStatePrivate = () => {
        this.setState({
            src: PrivateICon,
            alt: "PrivateIcon",
            statusSetting: "priv",
            name: "Chỉ mình tôi",
            class: "PrivateImageIcon",
        });

    };
    setStatePublic = () => {
        this.setState({
            src: PublicImage,
            alt: "PublicIcon",
            statusSetting: "pub",
            name: "Công khai",
            class: "PublicImageIcon",
        });
    };
    setStateFriend = () => {
        this.setState({
            src: FriendIcon,
            alt: "FriendIcon",
            statusSetting: "friend",
            name: "Bạn bè",
            class: "FriendImageIcon",
        });
    };
    OpenfileDialog = () => {
        document.getElementById("FileBrowser").click();
        document.getElementById("btnImage").disabled = true;
        document.getElementById("FileBrowser").disabled = true;

    }
    openTagsName = () => {
        if (this.state.tagsName && this.state.checkin) {
            this.setState({
                tagsName: false,
                checkin: false
            })
        }
        this.setState({
            tagsName: !this.state.tagsName,
            checkin: false,
            ContentWhoAndWhere: "Với",
            WithWhoOrWhere: "Cùng với ai?"
        })

    }
    openCheckIn = () => {

        this.setState({
            checkin: !this.state.checkin,
            tagsName: false,
            ContentWhoAndWhere: "Tại",
            WithWhoOrWhere: "Bạn đang ở đâu?"
        })
    }
    handleOnChange = (event) => {
        if (!event.target.files[0]) {
            document.getElementById("btnImage").disabled = false;
            document.getElementById("FileBrowser").disabled = false;

            return;
        }
        var imagefile = document.querySelector('#FileBrowser');
        for (var i = 0; i < imagefile.files.length; i++) {
            this.state.arrayImage.push(URL.createObjectURL(imagefile.files[i]))
        }
        this.setState({
            VisibleIMGdk: !this.state.VisibleIMGdk,
            tagsName: true,
        })
        this.setprogress();
    }
    setprogress = () => {
        var current_value = this.state.widthProgress;

        if (current_value >= 100) {
            return;
        } else {
            current_value++;
            this.setState({
                widthProgress: current_value
            })
            setTimeout(this.setprogress, 10)
        }
    }
    UpStatus = () => {
        var formData = new FormData();
        var imagefile = document.querySelector('#FileBrowser');
        if (document.getElementById("txtStatus1").value === "") {
            if (imagefile.files.length === 0) {
                document.getElementById("btnImage").disabled = false;
                document.getElementById("FileBrowser").disabled = false;

                window.alert("Bạn bắt buộc phải có caption hoặc hình ảnh/video")
                document.getElementById("txtStatus1").focus();
                return;
            }
        }

        if (imagefile.files.length !== 0) {

            for (var i = 0; i < imagefile.files.length; i++) {
                formData.append("file[]", imagefile.files[i]);
            }
            if (this.state.widthProgress < 100) {
                window.alert("Bạn phải đợi tiến trình tải hình hoàn tất thì mới có thể đăng status !!!");
                return;
            }
        }

        var params = {
            status_setting: this.state.statusSetting,
            caption: document.getElementById("txtStatus1").value,
            type: 0,
            option: 2
        }
        var route = "status/upload-status"
        var headers = {
            'content-type': 'multipart/form-data',
            Authorization: 'bearer' +
                localStorage.getItem("UserToken"),
        }
        var api = new API()

        api.onCallAPI('post', route, formData, params, headers).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message)
            } else {
                console.log(res.data.data)
                window.location.reload(false)
            }
        }).catch(err => {
            console.log(err)
        })
    }
    setImageUpTus = () => {

    }
    onshowImage() {

        return this.state.arrayImage.map(
            (image, index) => {
                return (< img src={image}
                    key={index}
                    style={
                        { marginLeft: "1vw", marginRight: "unset", height: "80px" }}
                    className="img-responsive"
                    alt="hình ảnh đính kèm"
                    width="100px"
                    height="100px" />)
            }
        )
    }
    render() {
        const styleProgressBar = {
            width: this.state.widthProgress + "%"
        }

        return (<form onSubmit={(e) => e.preventDefault()}>
            <div className="panel panel-default">
                {/* <!-- Default panel contents --> */}
                <div className="panel-heading">
                    <strong>Tạo bài viết</strong>
                </div>
                <div className="panel-body pnStatus">
                    <div className="ImageStatus">
                        <img
                            src={localStorage.getItem("avatar")}
                            alt="Image1"
                            className="IMGStatus"
                            width="65px"
                            height="65px"
                            border-radius="50%"
                        />
                    </div>
                    <TextareaAutosize className="TxtStatus"
                        id="txtStatus1"
                        placeholder={localStorage.getItem("UserName") + " ơi, bạn đang nghĩ gì?"} />
                </div>
                <div>
                    <div>
                        {

                            (this.state.widthProgress <= 100 && this.state.widthProgress > 0) &&
                            <div className="progress progress1" >
                                <div id="mybar"
                                    className="progress-bar bg-dark" role="progressbar"
                                    style={styleProgressBar}
                                    aria-valuenow={this.state.widthProgress} aria-valuemin="0"
                                    aria-valuemax="100">
                                </div>
                            </div>}
                        {
                            (this.state.widthProgress === 100) && <div >
                                {this.setImageUpTus()}
                                <div className="scrollHorizontal">
                                    {this.onshowImage()}
                                </div>

                            </div>
                        }
                        {
                            (this.state.tagsName) &&
                            <div className="padding5px">
                                <table className="tableTagsName" border="1" cellPadding="2" cellSpacing="2">
                                    <tbody>
                                        <tr>
                                            <td className="tdTagsName">
                                                <div className="divHaveVoi">
                                                    {this.state.ContentWhoAndWhere}
                                                </div>
                                            </td>
                                            <td>
                                                <input type="text" placeholder={this.state.WithWhoOrWhere}
                                                    id={this.state.idTagsName} className="CungVoiAi" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        }
                        {
                            (this.state.checkin) &&
                            <div className="padding5px">
                                <hr />
                                <table className="tableTagsName" border="1" cellPadding="2" cellSpacing="2">
                                    <tbody>
                                        <tr>
                                            <td className="tdTagsName">
                                                <div className="divHaveVoi">
                                                    {this.state.ContentWhoAndWhere}
                                                </div>
                                            </td>
                                            <td>
                                                <input type="text" placeholder={this.state.WithWhoOrWhere} id={this.state.idTagsName} className="CungVoiAi" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        }
                    </div>
                    <hr />
                    <div style={{display:"flex", justifyContent:"space-around", overflow:"hidden"}}>
                        <button type="button" id="btnImage"
                            className="btn btn-large btn-block btn-default btnAddimgVid btnInfoStt"
                            style={{ borderRadius: "16px" }}
                            onClick={this.OpenfileDialog}>
                            <img src={fbPictureAdd} className="imgAddIMG" alt="Hình ảnh" />
                            <strong>Ảnh/Video</strong>
                        </button>
                        <button type="button"
                            className="btn btn-large btn-block btn-default btnAddimgVid btnInfoStt"
                            style={{ borderRadius: "16px" }}
                            onClick={this.openTagsName}>
                            <img src={tagName} className="imgAddIMG" alt="Gắn thẻ" />
                            <strong>Gắn thẻ bạn bè</strong>
                        </button>
                        <button type="button"
                            className="btn btn-large btn-block btn-default btnAddimgVid btnInfoStt"
                            style={{ borderRadius: "16px" }}
                            onClick={this.openCheckIn}>
                            <img src={marker} className="imgAddIMG" alt="CheckIn" />
                            <strong>Check in</strong></button>

                        <input type="file" id="FileBrowser" multiple onChange={this.handleOnChange}
                            accept="image/*"
                            style={{ display: "none" }} />
                        {/* <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3 btnInfoStt">
                        </div> */}
                        <div className="dropdown" style={{width:"fit-content", float:"left", marginLeft:"8px"}}>
                            <a id="TrangThaibtn" role="button" href="# "
                                className="btn btn-primary dropdown-toggle  sttBtnCap"
                                type="button" data-toggle="dropdown"
                                style={{
                                    background: "lightgray", border: "unset"
                                }}
                            >
                                {/* #0055a8 */}
                                <img
                                    src={this.state.src}
                                    alt={this.state.alt}
                                    className={this.state.class}
                                    width="17px"
                                    height="17px"
                                />&nbsp; {this.state.name}
                            </a>
                            <ul className="dropdown-menu">
                                <li><button
                                    type="button"
                                    className="btnPublic"
                                    onClick={() => this.setStatePublic()}
                                >
                                    <img
                                        src={PublicImage}
                                        alt="Public"
                                        className="PublicImageIcon"
                                        width="17%" value="pub"
                                        height="17%"
                                    />
                                            &nbsp; Công khai
                                            </button></li>
                                <li><button
                                    type="button"
                                    className="btnFriend" value="friend"
                                    onClick={() => this.setStateFriend()}
                                >
                                    <img
                                        src={FriendIcon}
                                        alt="Friend"
                                        className="FriendImageIcon"
                                        width="17%"
                                        height="17%"
                                    />
                                        Bạn bè
                                        </button></li>
                                <li><button
                                    type="button"
                                    className="btnPrivate"
                                    onClick={() => this.setStatePrivate()}
                                >
                                    <img
                                        src={PrivateICon}
                                        alt="Private" value="priv"
                                        className="PrivateImageIcon"
                                        width="17%"
                                        height="17%"
                                    />
                                        Chỉ mình tôi
                                        </button></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <table className="table">
                    <tbody>
                        <tr>
                            <td>
                                <button id="btnDangStatus" onClick={() => this.UpStatus()} className="btnDangSTT" >
                                    Đăng
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
        );
    }

}

export default StatusContent;