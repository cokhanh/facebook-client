import React, { useState, useEffect } from 'react';
import io from 'socket.io-client';

let socket;

const ChatBox = () => {
    const ENDPOINT = 'http://localhost:5000';

    useEffect(() => {
        socket = io(ENDPOINT);
    });

    return (
        <h1>Chat box</h1>
    );
}

export default ChatBox;