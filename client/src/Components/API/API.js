import axios from 'axios'

class API {
    constructor() {
        // this.domain = 'https://api-tlcn.herokuapp.com/api/'
        // this.domain = 'http://172.16.31.89:8000/api/'
        this.domain ="http://13.76.199.59/api/"
        //this.domain = "http://localhost:8000/api/" 

    }
    onCallAPI = (method,url,data = {},params={},headers={}) => {
        return axios({
            method: method,
            url: this.domain + url,
            data: data,
            params : params,
            headers: headers
        });
    }
}

export default API