import React, { Component } from 'react';
import API from '../API/API';
import ContentStatusNoImg from '../ContentStatus/ContentStatusNoImg.js';
import './UnderFanpages.css';
import PublicImage from "../HinhAnh/Public.png";
import avatarPages from '../HinhAnh/NgoLanHuong1.jpg'
import coverPages from '../HinhAnh/NgoLanHuong2.jpg'
import StatusContent from "../UpStatus/StatusContent.js";

class UnderFanpages extends Component {
    constructor(props) {
        super(props)
        this.state = {
            Cover: "",
            avatar: "",
            avatarOld: avatarPages,
            coverOld: coverPages,
            data: null,
            dataUser: null,
            frmDataAvatar: new FormData(),
        }
    }
    saveAvatar = () => {
        var param = {
            update_type: 2,
            type: 0,
            option: 0, //1 là cover, avartar là 0
        }
        var route = "user/update/info";
        var headers = {
            Authorization: "bearer" + localStorage.getItem("UserToken")
        }

        var api = new API();
        api.onCallAPI('post', route, this.state.frmDataAvatar, param, headers).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message);
                this.cancelAvatar();

            } else {
                localStorage.setItem("avatar", res.data.data)
                this.setState({
                    changingAvatar: false
                })
                console.log(res.data.data);
            }
        }).catch(err => {
            console.log(err);
        })
    }
    saveAvatarPages = () => {
        var avatarNew = this.state.avatar;
        this.setState({
            avatar: "", avatarOld: avatarNew, changingAvatar: false, changeAvatar: false
        })
    }
    handleChangeAvatar = (e) => {
        var { frmDataAvatar } = this.state
        if (e.target.files[0]) {
            frmDataAvatar.append("file", e.target.files[0]);
            this.setState({
                avatar: URL.createObjectURL(e.target.files[0]),
                frmDataAvatar: frmDataAvatar,
                changingAvatar: true
            })
        }
    }
    cancelAvatar = () => {
        this.setState({
            avatar: "", changingAvatar: false, changeAvatar: false,
        });
    }
    componentDidMount = () => {
        //this.GetStatus();
        this.fillIntro();
    }
    fillIntro = () => {
        const params = {
            type_search: '1',
            token: localStorage.getItem("UserToken"),
        }
        const route = 'user/search-v1'
        const headers = {
            Authorization: 'bearer' +
                localStorage.getItem("UserToken")
        }
        var api = new API()
        api.onCallAPI('get', route, {}, params, headers).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message)
            } else {
                console.log(res.data.data)
                this.setState({
                    dataUser: res.data.data,
                })

            }
        }).catch(err => {
            console.log(err)
        })
    }
    GetStatus = () => {
        var params = {
            user_id: this.props.userId,
            type_search: 1
        }
        var route = 'status/load-personal-status'
        var headers = {
        }
        var api = new API()
        api.onCallAPI('get', route, {}, params, headers).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message)
            } else {
                this.setState({
                    data: res.data.data
                })
                console.log(res.data.data)
            }
        }).catch(err => {
            console.log(err)
        })
    }
    ShowStatus = () => {
        var { data } = this.state;
        if (data) {
            return data.map((user, index) => {

                return < ContentStatusNoImg
                    notOwn="true" key={index} indexTus={index} header={user.header_content}
                    statusId={user.id} liked={user.liked} whoLike={user.who_liked_status}
                    name={user.user_name} textid={"textCmt" + user.id}
                    srcAVT={user.user_avatar} caption={user.caption} likes={user.like_number}
                    srcCaption={user.posted_image} statusSetting={user.status_setting} />
            })
        }
    }
    showDataUser = () => {
        var { dataUser } = this.state;
        if (dataUser) {
            return (
                <div>
                    <div style={{overflow:"hidden"}}>
                        <i className="fas fa-thumbtack iconInfo"></i>
                        <p className="paragraphInfo"> {this.state.dataUser.email}</p>
                    </div>

                    <div style={{overflow:"hidden"}}>
                        <i className="fas fa-phone-alt iconInfo"></i>
                        <p className="paragraphInfo"> {this.state.dataUser.phone}</p>
                    </div>


                    <div style={{overflow:"hidden"}}>
                        <i className="fas fa-info-circle iconInfo"></i>
                        <p className="paragraphInfo"> {this.state.dataUser.dOb}</p>
                    </div>

                    <div style={{overflow:"hidden"}}>
                        <i className="fas fa-globe iconInfo"></i>
                        <p className="paragraphInfo"> {this.state.dataUser.sex ? "Nam" : "Nữ"}</p>
                    </div>

                </div>
            )
        }
    }
    render() {

        return (
            <div className="container pagesWall noPadding">
                <div className="coverPageArea">
                    <img src={coverPages} className="coverPages" alt="coverPage" />
                </div>
                <div className="row" style={{ paddingBottom: "16px" }}>
                    <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2 avatarPageArea">
                        <div
                            onMouseEnter={() => this.setState({
                                changeAvatar: true,
                            })}
                            onMouseLeave={() => this.setState({
                                changeAvatar: false,
                            })}>
                            <img src={this.state.avatar ? this.state.avatar : this.state.avatarOld}
                                className="img-responsive avatarPages "
                                alt="Image1"
                                style={{ borderRadius: "100%" }}

                            />
                            {this.state.changeAvatar && !this.state.changingAvatar && !this.state.coverChanging && <div>
                                <button
                                    className="btn btn-default btnChangeAva" style={{ top: "57px", left: "21px", width: "8vw", marginTop: "unset" }}
                                    onClick={() => {
                                        document.getElementById("fileAvatar").click();

                                    }}>
                                    <i className="fas fa-camera"></i>
                                    <br /> Cập nhật
                                    </button>
                                <input type="file" accept='image/*' style={{ display: "none" }} id="fileAvatar" onChange={this.handleChangeAvatar} />
                            </div>}
                        </div>
                    </div>

                    <div className="col-xs-10 col-sm-10 col-md-10 col-lg-10 infoPage noPadding  ">
                        <div className="pageName">
                            <h2>Tên fanpages</h2>
                            <span>Thể loại của Page</span>
                        </div>
                    </div>
                </div>
                <div className="navbar NAVinTimeline" style={{ background: "transparent", padding: "unset" }}>
                    {this.state.changingAvatar && <div>
                        <button className="btn btn-info" onClick={() => this.saveAvatarPages()}
                            style={{ margin: "10px", float: "right", backgroundColor: "#2d88FF" }}>Lưu thay đổi</button>
                        <button onClick={() => this.cancelAvatar()}
                            className="btn btn-default" style={{ margin: "10px 0", float: "right", }}>Hủy</button>
                        <span style={{ margin: "10px", float: "right", backgroundColor: "transparent" }} className="btnSaveCover lblCongKhaiCover">
                            <strong>Công khai</strong>
                        </span> </div>}
                    {(this.state.coverChanging) && <div className="container-fluid divBTNSaveCover">
                        <button className="btn btnSaveCover"
                            onClick={() => this.saveCover()}>
                            <strong> Lưu thay đổi </strong>
                        </button>
                        <button
                            onClick={() => this.onHadleCancelCover()}
                            className="btn btnSaveCover btnHuyCover"
                        >
                            <strong> Huỷ </strong>
                        </button>
                        <span className="btnSaveCover lblCongKhaiCover">
                            <strong>Công khai</strong>
                        </span>
                    </div>}
                    <ul className="nav navbar-nav ulNavTimeline" style={{}}>
                        {(!this.state.coverChanging) && <li>
                            <a href="# " onClick={() => this.setState({
                                showTimeline: true,
                                showFriendlist: false,
                            })} className="tagAinTimeLine"><strong>Trang chủ</strong></a>
                        </li>}

                        {(!this.state.coverChanging) && <li>
                            <a href="# " id="friendProfile" role="button" className="tagAinTimeLine"
                                onClick={() => {
                                    this.setState({
                                        showTimeline: false,
                                        showFriendlist: true,
                                        filter: null
                                    });
                                }}><strong>Giới thiệu </strong> {this.state.friend ? this.state.friend : ""}</a>
                        </li>}
                        {(!this.state.coverChanging) && <li>
                            <a href="# " className="tagAinTimeLine"><strong>Ảnh</strong></a>
                        </li>}

                    </ul>
                </div>
                <div className="row">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div className="row row1Intro ">
                            <div className="col-xs-1 col-sm-1 col-md-1 col-lg-1 divImageIntroHeader">
                                <img src={PublicImage} className="img-responsive imageIntro" alt="Image1" />
                            </div>
                            <p style={{ fontSize: "1.5vw", marginBottom: "unset", fontWeight: "700", color: "#3578E5" }}>
                                Giới thiệu
                             </p>
                            {this.showDataUser()}
                        </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                        <StatusContent />
                    </div>
                </div>
                <div></div>
                <div></div>

            </div>


        );
    }
}

export default UnderFanpages;
