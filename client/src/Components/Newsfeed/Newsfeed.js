import React, { Component } from 'react';
import './newsfeed.css'; //import toan bo class bên app css qua
import API from '../API/API';
import ContentStatusNoImg from '../ContentStatus/ContentStatusNoImg';
import DashBar from './DashBar';
import StatusContent from '../UpStatus/StatusContent.js';
import LazyLoad from 'react-lazyload';
class Newsfeed extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            is_visible: false,
            lltodo: null
        };
    }

    componentDidMount = () => {
        this.getStatusNF();
    }

    getStatusNF = () => {
        var param = {
            token: localStorage.getItem("UserToken")
        }
        var route = "status/news-feed"
        var header = {
            Authorization: "bearer" + localStorage.getItem("UserToken")
        }
        var api = new API();
        api.onCallAPI('get', route, {}, param, header).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message)
            }
            else {
                this.setState({
                    data: res.data.data
                })
            }
        }).catch(err => {
            console.log(err)
        })
    }
    randomArray(array) {
        let i = array.length - 1;
        for (; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            const temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    }
    onShowStatus = () => {
        var users = this.state.data;
        // var users1 = this.state.data;
        // if (users1) {
        //     users1.map((user, index) => {
        //         var d = (user.posted_time)
        //         return console.log(d)
        //     })
        // }

        if (users) {
            return users.map((user, index) => {
                return (<LazyLoad height={100} offset={100} key={index}>
                    <ContentStatusNoImg notOwn="true" key={index}
                        indexTus={index} header={user.header_content}
                        statusId={user.id} liked={user.liked}
                        whoLike={user.who_liked_status} name={user.user_name}
                        textid={"textCmt" + user.id} srcAVT={user.user_avatar}
                        caption={user.caption} likes={user.like_number}
                        srcCaption={user.posted_image}
                        statusSetting={user.status_setting} />
                </LazyLoad>)
            })
        }
    }
    render() {
        return (
            <div>
                <DashBar />
                <div className="container DashBarNAV" align="left">
                </div>
                <div className="container mainWall">
                    <div className="createSTT">
                        <StatusContent marginLeft="22%" />
                    </div>
                    <br />
                    {this.onShowStatus()}
                </div>

            </div >


        );
    }
}

export default Newsfeed;
