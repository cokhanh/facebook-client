import React from 'react';
import TextareaAutosize from 'react-textarea-autosize';
import '../css/Input.css';
import sendIcon from '../../../HinhAnh/sendIcon.png'

const Input = ({ setMessage, sendMessage, message, botFunction, room }) => {
  
  return (<form className="form1" >
    <TextareaAutosize className="input1" id="input-message"
      placeholder="Nhập tin nhắn..." value={message}
      onChange={({ target: { value } }) => {
        setMessage(value);
        botFunction();
      }}
      onKeyPress={(event) => event.key === 'Enter' ? sendMessage(event) : null}
    />
    <button className="sendButton" style={{ backgroundColor: "transparent" }}
      onClick={e => sendMessage(e)}>
      <img src={sendIcon} style={{ width: "36px", height: "36px" }} className="img-responsive" alt="Send" />
    </button>
  </form>)
}

export default Input;