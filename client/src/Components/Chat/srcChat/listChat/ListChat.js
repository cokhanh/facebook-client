import React, { useEffect, useState } from 'react';
import '../css/ListChat.css';
import API from '../../../API/API';

export default function SignIn() {
  const [friendmess, setFriendmess] = useState([]);
  function clearText() {
    if (document.getElementById("txtSearchMess").value === "") {
      document.getElementById("clearSearch").style.display = "none";
    }
    else
      document.getElementById("clearSearch").style.display = "block"
  }

  useEffect(() => {
    var chatroom = window.location.href;
    var chatroomId = (chatroom.substring(chatroom.lastIndexOf("=") + 1));
    var chatroomMount = document.getElementById(chatroomId);
    if (chatroomMount) {
      chatroomMount.style.backgroundColor = "rgba(45, 136, 255, 0.1)";
      document.getElementById("chatRoomGroup").scrollTop = chatroomMount.offsetTop
    }
  }, [])

  useEffect(() => {
    var route = "chat/list-messages"
    var param = {
      token: localStorage.getItem("UserToken")
    }
    var header = {
      Authorization: "bearer" + localStorage.getItem("UserToken")
    }
    var api = new API();
    api.onCallAPI('get', route, {}, param, header).then(res => {
      if (res.data.error_code !== 0) {
        window.alert(res.data.message)
      }
      else {
        if (res.data.data) {
          setFriendmess(res.data.data);
          console.log(res.data.data);
        }
      }
    })
  }, [])
  let elementFriend = friendmess.map((user, i) => {
    return (
      <div key={i} id={user.room} className="chat-hover">
        <a href={`/chat?room=${user.room} `} id={"chatroom" + i + "chatbot"}
          onClick={(e) => {
            if (!user.userName || !user.room)
              e.preventDefault();
            else {
              localStorage.setItem("sender-messenger-avatar", user.avatar)
              localStorage.setItem("sender-messenger-link", user.partner_no_sign_profile)
              localStorage.setItem("sender-messenger-name", user.friend_chat)
              return null;
            }
          }}
        >
          <div className="message-friend" onClick={
            () => localStorage.setItem("sender-messenger-avatar", user.avatar)
          }>
            <div>
              <img src={user.avatar}
                className="avatar-message" alt="avartar" />
            </div>

            <div className="content-message">
              <span className="userName-message">{user.friend_chat}</span><br />
              <div className="recent-message">
                {user.isCurrent ? "Bạn:" : user.friend_chat + ":"} {user.messRecent}
              </div> <br />
              <span style={{ float: "left" }}>
                {user.time}
              </span>
            </div>
          </div>
        </a>
      </div>

    )
  })

  return (
    <div className="joinOuterContainer">
      <div className="heading1">
        <h1>Chat</h1>
      </div>
      <div className="search-form">
        <i className="fal fa-search search-form__icon"></i>
        <input type="text" onChange={() => clearText()} id="txtSearchMess"
          placeholder="Tìm kiếm trên Messenger"
          className="search-form__input" />
        <i id="clearSearch"
          onClick={() => {
            document.getElementById("txtSearchMess").value = ""
            document.getElementById("clearSearch").style.display = "none"
          }}
          style={{ display: "none" }} className="fas fa-times"></i>
      </div>
      <div className="message-friend-group" id="chatRoomGroup">
        {elementFriend}
      </div>
    </div>
  );
}
