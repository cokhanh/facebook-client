import React from 'react';
import { Link } from "react-router-dom";
import '../css/InfoBar.css';
var avatarFriend = localStorage.getItem("sender-messenger-avatar");
const InfoBar = ({ room }) => (

  <div className="infoBar">
    <div className="leftInnerContainer">
      <img src={avatarFriend} className="img-responsive avatar-message"
        alt="Avatar" />
      <Link to={"/" + localStorage.getItem("sender-messenger-link")} >
        <span className="spanInfoBar">{localStorage.getItem("sender-messenger-name")}</span>
      </Link>
      <span></span>
    </div>
  </div >
);

export default InfoBar;