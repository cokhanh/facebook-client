import React from 'react';

import ReactEmoji from 'react-emoji';
import './Message.css'

const Message = ({ message: { text, user, avatar }, name }) => {
  let isSentByCurrentUser = false;

  const trimmedName = name.trim().toLowerCase();

  if (user === trimmedName) {
    isSentByCurrentUser = true;
  }

  return (
    isSentByCurrentUser
      ? (
        <div className="messageContainer justifyEnd">
          <div className="messageBox" style={{ backgroundColor: "rgb(110, 223, 0)" }}>
            <p className="messageText" style={{ color: "white" }}>{ReactEmoji.emojify(text)}</p> {/**/}
          </div>

        </div>
      )
      : (
        <div className="messageContainer justifyStart">
          <img src={avatar} className="imageChat" alt="Image1" /> {/* avatar của bạn chat */}
          <div className="messageBox backgroundLight">
            <p className="messageText colorDark">{ReactEmoji.emojify(text)}</p>
          </div>
        </div>
      )
  );
}

export default Message;