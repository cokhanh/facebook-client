import React, { useEffect } from 'react';
import Message from './Message/Message';
import '../css/Messages.css';

const Messages = ({ messages, name, setVisible }) => {
  var objDiv = document.getElementById("mess-group");
  var maxHeight;
  if (objDiv) {
    maxHeight = objDiv.scrollHeight;
  }
  useEffect(() => {
    var groupmess = document.getElementById("mess-group");
    if (groupmess) {
      groupmess.scrollTop = document.getElementById("mess-group").scrollHeight;
    }
  }, [maxHeight])
  function checkScroll() {
    if (objDiv) {
      if ((objDiv.scrollTop) <= objDiv.scrollHeight - objDiv.clientHeight - 1)
        setVisible(true)
      else setVisible(false)
    }
  }
 // console.log(messages)

  return (
    <div className="messages"
      onClick={() => {
        if (document.getElementById("input-message")) {
          document.getElementById("input-message").focus()
        }
      }}
      onScroll={() => checkScroll()} id="mess-group" >
      {
        messages.map((message, i) => <div key={i}>
          <Message message={message} name={name} /></div>)
      }
    </div >)
};

export default Messages; 