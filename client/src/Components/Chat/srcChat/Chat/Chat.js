import React, { useState, useEffect } from "react";
import queryString from 'query-string';
import io from "socket.io-client";
import API from '../../../API/API';
import Messages from '../Messages/Messages';
import Input from '../Input/Input';
import InfoBar from '../InfoBar/InfoBar.js';
import ListChat from '../../../Chat/srcChat/listChat/ListChat.js';
import '../css/Chat.css';

const ENDPOINT = 'http://localhost:5000';

let socket;
console.log(2);
const Chat = ({ location, userName }) => {
  const [name, setName] = useState('');
  const [room, setRoom] = useState('');
  const [message, setMessage] = useState('');
  const [messages, setMessages] = useState([]);
  const [visible, setVisible] = useState(false);
  //const [firstMess, setfirstMess] = useState()

  socket = io(ENDPOINT);
  if (window.location.href.endsWith('/chat')) {
    if (document.getElementById("chatroom0chatbot"))
      document.getElementById("chatroom0chatbot").click(); //đẩy 2 cái đó vô name room chỗ này đc chưa
  }
  useEffect(() => {
    var route = "chat/list-messages-in-group";
    var param;
    if (room) {
      param =
      {
        chat_group_id: room,
      };
      var api = new API();
      api.onCallAPI('get', route, {}, param, {}).then(res => {
        if (res.data.error_code !== 0) {
          window.alert(res.data.message)
        }
        else {
          if (res.data.data) {
            console.log(res.data.data);
            res.data.data.map((mess) => {
              var messageText = {
                user: mess.owner,
                text: mess.contents,
                avatar: mess.user_avatar
              };
              setMessages(messages => [...messages, messageText]);
              //setName(mess.owner)
              return 0;
            })
            //botFunction();
          }
        }
      })
    }

  }, [room])
  useEffect(() => {
    const { room } = queryString.parse(location.search); //lấy 2 cái đó set vô đây
    const name = localStorage.getItem("linkProfile");

    setRoom(room);
    setName(name)
    //
    if (room) {
      socket.emit('join', { name, room }, () => {
        console.log(name, room)
      });
    }
  }, [location.search]);
  useEffect(() => {
    socket.on('message', message => {
      var messageText = {
        user: message.user,
        text: message.text,
        avatar: localStorage.getItem("avatar")
      }
      setMessages(messages => [...messages, messageText]);
    });
  }, []);


  const sendMessage = (event) => {
    event.preventDefault();
    if (message.trim() === "")
      return;
    if (message) {
      socket.emit('sendMessage', message, name, () => setMessage(''));
    }
    if (document.getElementById("mess-group")) {
      document.getElementById("mess-group").scrollTop = document.getElementById("mess-group").scrollHeight;
    }
    if (message) {
      var route = "chat/save-message"
      if (message.trim() === "") {
        return;
      }
      var param = {
        chat_group_id: room,
        content: message
      }
      var header = {
        Authorization: "bearer" + localStorage.getItem("UserToken")
      }
      var api = new API();
      api.onCallAPI('post', route, {}, param, header).then(res => {
        if (res.data.error_code !== 0) {
          window.alert(res.data.message)
        }
        else {
          if (res.data.data) {
            console.log(res.data.data);
          }
        }
      })
    }
  }
  var objDiv = document.getElementById("mess-group");

  function botFunction() {
    if (objDiv) {
      setVisible(false);
      objDiv.scrollTo({
        behavior: "smooth", top: objDiv.scrollHeight
      })
    }
  }
  return (
    <div className="outerContainer" >
      <div className="container2">
        <ListChat />
      </div>
      <div className="container1">
        <InfoBar room={room} />
        <Messages messages={messages} name={name} setVisible={setVisible} />
        <div style={{ margin: "auto" }}>
          {visible && < button className="btn btn-default" id="btnScrollDown"
            onClick={() => botFunction()}
          >
            <i className="fas fa-long-arrow-alt-down" style={{
              fontSize: "15px",
            }}></i>
          </button>}
        </div>

        <Input message={message} room={room} setMessage={setMessage} botFunction={botFunction} sendMessage={sendMessage} />
      </div>
    </div >
  );
}

export default Chat;
