import React, { Component } from 'react'
import './ContentStatus.css'
import fbLikeIcon from "../HinhAnh/fbLikeIcon.png";
import fbCmtIcon from "../HinhAnh/fbCmtIcon.png";
import fbShareIcon from "../HinhAnh/fbShareIcon.png";
import fbLikeBlue from "../HinhAnh/fbLikeBlue.png"
import CommentInStt from './CommentInStt.js'
import API from '../API/API.js';
class DivHaveCLS extends Component {
    constructor(props) {
        super(props)
        this.state = {
            src: this.props.liked ? fbLikeBlue : fbLikeIcon,
            checkLike: this.props.liked ? true : false,//Chua like
            txtCMT: "",
            showHidenComment: false,
            likeNum: parseInt(this.props.likes),
            data: this.props.whoLike
        }
    }
    showUserLike = () => {
        var userLike = this.state.data.user_name ? this.state.data.user_name : "";
        var length = this.state.data ? this.state.data.length : 0;
        if (length === 0) {
            return
        }

        for (var i = 0; i < length; i++) {
            userLike += this.state.data[i].user_name + ", "
        }
        if (length > 2) {
            userLike = (this.state.data[0].user_name + ", " + this.state.data[1].user_name +
                " và " + (length - 2) + " người khác")
        }
        const iconLike = "data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e"
        return (
            <div className="positionLike" id="positionLike">
                <img src={iconLike} style={{ display: "inline-block", width: "18px", height: "18px" }} className="img-responsive" alt="Image1" />
                <a role="button" id="userLikes" href={"#modalUserLike" + this.props.index}
                    data-toggle="modal"
                >
                    {
                        (length <= 2 && length > 0) &&
                        userLike.substring(0, userLike.lastIndexOf(','))

                    }

                    {
                        length > 2 && userLike

                    }
                </a>
                <div className="modal fade" id={"modalUserLike" + this.props.index} tabIndex="-1" role="dialog" aria-labelledby="modalUserLikeLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <img src={iconLike} style={{ display: "inline-block", width: "18px", height: "18px" }}
                                    className="img-responsive" alt="Image1" />
                                <span style={{ fontWeight: "900" }}>&nbsp;{length}</span>
                                <button type="button" className="close closeBtn" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                {
                                    this.state.data.map((user, index) => {
                                        return (
                                            <div key={index} style={{ marginBottom: "5px" }}>
                                                <img src={user.avatar} className="img-responsive"
                                                    style={{
                                                        width: "38px", height: "38px", border: "1px solid", margin: "5px",
                                                        borderRadius: "100%", display: "inline-block"
                                                    }}
                                                    alt="userAvatar" />
                                                <p style={{ display: "inline-block", fontSize: "15px", fontWeight: "700" }}>{user.user_name}</p>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    Like = () => {
        if (this.state.checkLike === false) { //like (checklike = fasle la chua like)
            var { likeNum } = this.state
            this.setState({
                src: fbLikeBlue,
                className: "h5-btnLiked",
                checkLike: true, // da like
                likeNum: likeNum + 1
            })
            console.log(likeNum + 1)
            var route = "status/update-status"
            var param = {
                status_id: this.props.statusId,
                like: likeNum + 1,
            }
            var header = {
                Authorization: 'bearer' + localStorage.getItem("UserToken")
            }
            var api = new API();
            api.onCallAPI('post', route, {}, param, header).then(res => {
                if (res.data.error_code !== 0) {
                    window.alert(res.data.message)
                } else {
                    this.setState({ data: res.data.data })
                }
            }).catch(err => {
                console.log(err)
            })
        }
        else { //unlike
            var likeNum1 = this.state.likeNum
            this.setState({
                src: fbLikeIcon,
                className: "h5-btnLike",
                checkLike: false,
                likeNum: likeNum1 - 1
            })
            console.log(likeNum1 - 1)
            var params = {
                status_id: this.props.statusId,
                like: likeNum1 - 1,
                is_unlike: 1,
            }
            var headers = {
                Authorization: "bearer" + localStorage.getItem("UserToken")
            }
            var routes = "status/update-status"
            var api1 = new API();
            api1.onCallAPI('post', routes, {}, params, headers).then(res => {
                if (res.data.error_code !== 0) {
                    window.alert(res.data.message)
                } else {
                    this.setState({ data: res.data.data })
                    console.log(res.data.data)
                }
            }).catch(err => {
                console.log(err)
            })
        }

    }

    getfocus = () => {
        document.getElementById(this.props.textid).focus();
    };
    handleSubmit = (event) => {
        event.preventDefault();
        return (<CommentInStt comment={this.state.txtCMT} />);
    }

    onKeyDown = (e) => {
        if (e.key === 'Enter') {
            var target = e.target;
            var name = target.name;
            var value = target.value;
            this.setState({
                [name]: value,
                showHidenComment: true
            });
            document.getElementById(this.props.textid).value = '';
        }
    }

    render() {
        var userCmt = [
            {
                userId: 1,
                username: "cơ khánh",
                cmt_content: "Xin chào, tôi tên là Cơ Khánh và tôi đến từ TPHCM"
            },
            {
                userId: 2,
                username: "Ngô Lan Hương",
                cmt_content: "Xin chào, tôi tên Hương và tôi đến từ Hà Nội"
            },
            {
                userId: 3,
                username: "Tường Khải",
                cmt_content: "xin chào, tôi tên Khải và tôi đến từ Đà Nẵng"
            }
        ];
        let element = userCmt.map((userCmt, index) => {
            return (
                <div key={index}>
                    <CommentInStt UserName={userCmt.username} comment={userCmt.cmt_content} />
                </div>)
        });
        return (
            <div>
                {this.showUserLike()}
                <div className="KhungChuaLCS">
                    <button type="button" className="btn btnLike" onClick={() => this.Like()}>
                        <h5 className="h5-btnLike">
                            <img src={this.state.src} alt="Like" style={{ marginRight: "5px" }} width="15%" height="15%" />
                                Thích {this.state.likeNum ? (" " + this.state.likeNum) : ""}
                        </h5>
                    </button>
                    <button type="button" className="btn btnCmt" onClick={() => this.getfocus()} >
                        <h5 className="h5-btnCmt" >
                            <img src={fbCmtIcon} alt="cmt" style={{ marginRight: "5px" }} width="15%" height="15%" />
                                    Bình luận
                                </h5>
                    </button>
                    <button type="button" className="btn btnShare" >
                        <h5 className="h5-btnShare">
                            <img src={fbShareIcon} alt="Share" style={{ marginRight: "5px" }} width="15%" height="15%" />
                                    Chia sẻ
                                </h5>
                    </button >
                </div>
                {element}

                {this.state.showHidenComment && <div><br /> <CommentInStt UserName={this.props.name} comment={this.state.txtCMT} /></div>}
                <br />
                <form onSubmit={(e) => e.preventDefault()}>
                    <div className="container-fluid InfoCaption"> {/*  Khung chứa thông tin người đăng caption */}
                        <div className="col-sm-1 imgOfInfo">
                            <img
                                src={localStorage.getItem("avatar")}
                                width="40px"
                                height="40px"
                                className="ImageProfile "
                                alt="facebook Profile Image1" />
                        </div>
                        <div className="col-sm-10 NameAndStatus txtcmttus">
                            <input type="text" id={this.props.textid}
                                name="txtCMT"
                                className="form-control txtCmt"
                                placeholder="Viết bình luận"
                                onKeyDown={this.onKeyDown}
                            />

                        </div>

                    </div>
                </form>

            </div>
        );
    }
}
export default DivHaveCLS;