import React, { Component } from 'react'
import './ContentStatus.css'
import NgoLanHuong1 from "../HinhAnh/NgoLanHuong1.jpg"
class CommentInStt extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
    }
    render() {
        return (
            <div style={{ marginTop: "5px" }}> {/*  Khung chứa thông tin người đăng caption */}
                <div className="col-sm-1" style={{ paddingLeft: "unset", paddingRight: "unset" }}>
                    <img
                        src={NgoLanHuong1}
                        width="40px"
                        height="40px"
                        className="ImageProfile"
                        alt="facebook Profile Image1" style={{ float: "left" }} />
                </div>

                <div className="col-sm-11 InfoComment txtCmt">
                    <span>
                        <a href="# " style={{ fontWeight: "600" }}>
                            {this.props.UserName} <br />
                        </a>
                        &nbsp;
                        <span>
                            <span>
                                <span style={{ fontSize: "16px" }}>
                                    {this.props.comment}
                                </span>
                            </span>
                        </span>
                    </span>
                </div>
            </div>
        );
    }
}
export default CommentInStt;