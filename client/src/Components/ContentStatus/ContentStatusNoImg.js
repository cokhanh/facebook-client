import React, { Component } from "react";
import PublicImage from "../HinhAnh/Public.png";
import FriendIcon from "../HinhAnh/FriendIcon.png";
import PrivateICon from "../HinhAnh/PrivateICon.png";
import dropdownIcon from "../HinhAnh/sort_down.png";
import trashcan from "../HinhAnh/trash_can_25px.png";
import DivHaveLCS from "./DivHaveLCS.js";
import './ContentStatus.css'
import API from "../API/API";
class ContentStatusNoImg extends Component {
  constructor(props) {
    super(props);

    this.state = {
      src: PublicImage,
      alt: "PublicIcon",
      loadding: false,
      srcImage: [],
    };
  }
  setStatusfirst = (value) => {
    if (value === "pub") {
      this.setState({
        src: PublicImage,
        alt: "PublicIcon",
      });
      return;
    }
    if (value === "priv") {
      this.setState({
        src: PrivateICon,
        alt: "PrivateIcon",
      });
      return;
    }
    if (value === "friend") {
      this.setState({
        src: FriendIcon,
        alt: "FriendIcon",
      });
      return;
    }
  };
  componentDidMount = () => {
    this.setStatusfirst(this.props.statusSetting);
    if (this.props.notOwn === "true") {
      document.getElementById("dropdownIcon" + this.props.indexTus).style.display = "none";
      document.getElementById("trashStt" + this.props.indexTus).style.display = "none";
    }
  }
  clickSetting = () => {
    if (this.props.notOwn === "true") {
      document.getElementById("setting" + this.props.indexTus).className = "StatusofCaption";
    }
  }
  updateSettingStatus = (settingStatus) => {
    var route = "status/update-status";
    var params = {
      status_id: this.props.statusId,
      status_setting: settingStatus,
    };
    var headers = {
      Authorization: "bearer" + localStorage.getItem("UserToken"),
    };
    var api = new API();
    api.onCallAPI("post", route, {}, params, headers)
      .then((res) => {
        if (res.data.error_code !== 0) {
          window.alert(res.data.message);
        } else {
          this.LoaddedSetting();
          console.log(res.data.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  setStatePrivate = () => {
    this.updateSettingStatus("priv");
    this.setState({
      src: PrivateICon,
      alt: "PrivateIcon",
      class: "PrivateImageIcon",
    });
  };
  setStatePublic = () => {
    this.LoaddingSetting();
    this.updateSettingStatus("pub");
    setTimeout(this.LoaddedSetting, 3000);

    this.setState({
      src: PublicImage,
      alt: "PublicIcon",
      class: "PublicImageIcon",
    });
  };
  setStateFriend = () => {
    this.LoaddingSetting();
    this.updateSettingStatus("friend");
    this.setState({
      src: FriendIcon,
      alt: "FriendIcon",
      class: "FriendImageIcon",
    });
  };
  LoaddingSetting = () => {
    this.setState({
      loadding: true,
    });
  };
  LoaddedSetting = () => {
    this.setState({
      loadding: false,
    });
  };
  deleteStatus = () => {
    var route = "status/delete"
    var param = {
      status_id: this.props.statusId
    }
    var header = {
      Authorization: "bearer" + localStorage.getItem("UserToken")
    }
    var api = new API()
    api.onCallAPI("post", route, {}, param, header)
      .then((res) => {
        if (res.data.error_code !== 0) {
          window.alert(res.data.message);
        } else {
          //this.LoaddedSetting();
          console.log(res.data.data);
          window.location.reload(false);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    const styleBtnStatus = {
      width: "26px",
      paddingBottom: "2px",
    };

    return (
      <div className="panel panel-default">
        <div className="panel-body bodyPanel">
          <div>
            <div className="container-fluid InfoCaption">
            
              {/*  Khung chứa thông tin người đăng caption */}
              <div className="col-sm-1 ImgHeadingSTT" onClick={()=>window.location.href="/" + localStorage.getItem("linkProfile")}>
                <img
                  src={this.props.srcAVT}
                  width="40px"
                  height="40px"
                  className="ImageProfile"
                  alt="facebook Profile Image1" style={{ marginTop: "unset" }}
                />
              </div>

              <div className="col-sm-10 NameAndStatus">
                <span> </span>
                <a href="# ">
                  <strong> {this.props.name} </strong>
                </a>
                <span> {this.props.header}</span>

                <span> </span>
                <div>
                  <span className="label lblTgianDang">15 phút</span>

                  <div className="dropdown StatusofCaption" id={"setting" + this.props.indexTus}
                    onClick={() => this.clickSetting()}
                  >
                    <a
                      href="# "
                      className="btn btn-secondary dropdown-toggle sttBtnCapImg"
                      id="TrangThaibtn"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      <div style={styleBtnStatus}>
                        {!this.state.loadding && (
                          <div id="statusSetting">
                            <img
                              src={this.state.src}
                              alt={this.state.alt}
                              width="50%"
                            />
                            <img
                              src={dropdownIcon}
                              alt="dropdownIcon"
                              width="50%" id={"dropdownIcon" + this.props.indexTus}
                            />
                          </div>
                        )}
                        {this.state.loadding && (
                          <i
                            className="fa fa-circle-o-notch fa-spin"
                            id="loaddingSetting"
                          ></i>
                        )}
                      </div>
                    </a>
                    <div
                      className="dropdown-menu"
                      aria-labelledby="dropdownMenuButton"
                    >
                      <button
                        type="button"
                        className="btnPublic"
                        onClick={() => this.setStatePublic()}
                      >
                        <img
                          src={PublicImage}
                          alt="Public"
                          width="17%"
                          height="17%"
                        />
                        Công khai
                      </button>

                      <button
                        type="button"
                        className="btnFriend"
                        onClick={() => this.setStateFriend()}
                      >
                        <img
                          src={FriendIcon}
                          alt="Friend"
                          width="17%"
                          height="17%"
                        />
                        Bạn bè
                      </button>

                      <button
                        type="button"
                        className="btnPrivate"
                        onClick={() => this.setStatePrivate()}
                      >
                        <img
                          src={PrivateICon}
                          alt="Private"
                          width="17%"
                          height="17%"
                        />
                        Chỉ mình tôi
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div style={{ float: "right" }}>

                <button className="btn btn-large btn-block btn-default" id={"trashStt" + this.props.indexTus} style={{ padding: "unset" }}
                  data-toggle="modal" data-target={"#confirmDelete" + this.props.indexTus}
                >
                  <img style={{ width: "100%", height: "100%" }}
                    src={trashcan} className="img-responsive" alt="Image1" /></button>
                <div className="modal fade" id={"confirmDelete" + this.props.indexTus} tabIndex="-1" role="dialog"
                  aria-labelledby="confirmDeleteLabel" aria-hidden="true">
                  <div className="modal-dialog" role="document">
                    <div className="modal-content">
                      <div className="modal-header">
                        <h5 className="modal-title" id="confirmDelete">Cảnh báo !!!</h5>
                        <button type="button" className="close closeBtn" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div className="modal-body">
                        Bạn có chắc xoá status này không?
                      </div>
                      <div className="modal-footer" style={{ borderTop: "unset" }}>
                        <button type="button"
                          onClick={() => this.deleteStatus()}
                          className="btn btn-primary">Có</button>
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Không</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div>
              <p>{this.props.caption}</p>
              {this.CheckImgFromdb(this.props.srcCaption)}
            </div>
          </div>
          <hr />
          <DivHaveLCS
            index={this.props.indexTus}
            name={this.props.name}
            liked={this.props.liked}
            whoLike={this.props.whoLike}
            statusId={this.props.statusId}
            likes={this.props.likes}
            textid={this.props.textid}
          />
        </div>
      </div>
    );
  }

  deleteFirstImage(src) {
    let elements = src.map((imageSrc, index) => {
      return (
        <div className="item" id={"imageCap" + index} key={index}>
          <img
            loading="lazy"
            src={imageSrc}
            className="img-thumbnail"
            alt={"hinhAnh" + index}
            style={{ width: "100%", height: "450px" }}
          />
        </div>
      );
    });
    elements.shift();
    return elements;
  }
  CheckImgFromdb(src) {
    if (src.length === 0) {
      return <div></div>;
    } else {
      if (src.length === 1) {
        return (
          <img
            loading="lazy"
            src={src[0]}
            className="img-thumbnail"
            alt="hinhAnh1"
          />
        );
      }
      return (
        <div
          id={"myCarousel" + this.props.indexTus}
          className="carousel slide"
          data-ride="carousel"
        >
          <div
            className="carousel-inner"
            style={{
              height: "450px",
            }}
          >
            <div className="item active">
              <img
                loading="lazy"
                src={src[0]}
                className="img-thumbnail"
                alt={"hinhAnh"}
                style={{ width: "100%", height: "450px" }}
              />
            </div>
            {this.deleteFirstImage(src)}
          </div>
          <a
            className="left carousel-control"
            href={"#myCarousel" + this.props.indexTus}
            data-slide="prev"
          >
            <span className="glyphicon glyphicon-chevron-left"></span>
            <span className="sr-only">Previous</span>
          </a>
          <a
            className="right carousel-control"
            href={"#myCarousel" + this.props.indexTus}
            data-slide="next"
          >
            <span className="glyphicon glyphicon-chevron-right"></span>
            <span className="sr-only">Next</span>
          </a>
        </div>
      );
    }
  }
}

export default ContentStatusNoImg;
