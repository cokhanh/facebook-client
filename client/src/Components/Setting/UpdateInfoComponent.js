import React, { Component } from 'react';
import API from '../API/API'
class UpdateInfoComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            Editting: false,
            day: 0, month: 0, year: 0,
            visibleTagEmailNew: false,
            visibleOTP: false,
            visibleTagSdt: false,
            visibleDivEmail: true,
            data: null,
        }
    }
    updataInfo = (route, headers, params) => {
        var api = new API()
        api.onCallAPI('post', route, {}, params, headers).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message);
                document.getElementById("txtHo").value = this.state.data.last_name;
                document.getElementById("txtTen").value = this.state.data.first_name;
                document.getElementById("txtEmail").value = this.state.data.email;
                document.getElementById("txtSdt").value = this.state.data.phone;
                document.getElementById("dayBirth").value = this.state.day
                document.getElementById("monthBirth").value = this.state.month
                document.getElementById("yearBirth").value = this.state.year
            } else {
                console.log(res.data.data)
                document.getElementById("btnStart").click();

            }
        }).catch(err => {
            console.log(err)
        })
    }
    handleSubmitBirth = () => {
        var param
        if (document.getElementById("dayBirth").value === this.state.day &&
            document.getElementById("monthBirth").value === this.state.month &&
            document.getElementById("yearBirth").value === this.state.year) {
            alert("Bạn cần phải thay đổi thông tin ngày sinh!")
            return;
        }
        param = {
            dOb: document.getElementById("dayBirth").value + "/" +
                document.getElementById("monthBirth").value + "/" + document.getElementById("yearBirth").value,
            update_type: 0,
            password: document.getElementById("password2").value
        }
        var header = {
            Authorization: "bearer" + localStorage.getItem("UserToken")
        }
        var route = "user/update/info";
        document.getElementById("FixNation").className = "collapse"
        document.getElementById("loaddingBirth").style.display = "block"
        document.getElementById("loaddedBirth").style.display = "none"
        setTimeout(this.showLoadding, 5000);
        this.updataInfo(route, header, param);
        document.getElementById("password2").value = "";
    }
    handleSubmitLienHe = (e) => {
        var param
        if (document.getElementById("txtEmail").value === this.state.data.email &&
            document.getElementById("txtSdt").value === this.state.data.phone) {
            window.alert("Bạn phải thay đổi thông tin thì mới lưu được");
            return;
        }
        if (document.getElementById("txtEmail").value === this.state.data.email) {
            param = {
                phone: document.getElementById("txtSdt").value,
                update_type: 0,
                password: document.getElementById("password1").value,
            }
        }
        if (document.getElementById("txtSdt").value === this.state.data.phone) {
            param = {
                email: document.getElementById("txtEmail").value,
                update_type: 0,
                password: document.getElementById("password1").value,
            }
        }
        if (document.getElementById("txtSdt").value !== this.state.data.phone && document.getElementById("txtEmail").value !== this.state.data.email) {
            param = {
                email: document.getElementById("txtEmail").value,
                update_type: 0,
                password: document.getElementById("password1").value,
                phone: document.getElementById("txtSdt").value
            }
        }
        var route = "user/update/info";
        var header = {
            Authorization: 'bearer' + localStorage.getItem("UserToken")
        };
        document.getElementById("FixContact").className = "collapse"
        document.getElementById("loaddingContact").style.display = "block"
        document.getElementById("loaddedContact").style.display = "none"
        setTimeout(this.showLoadding, 5000);
        this.updataInfo(route, header, param);
        document.getElementById("password1").value = "";

    }

    onShowUserInfo = () => {
        var data = this.state.data;
        if (data) {
            if (localStorage.getItem("UserName") !== data.last_name + " " + data.first_name) {
                localStorage.setItem("UserName", data.last_name + " " + data.first_name);
                window.location.reload(false);
            }
            return (
                <div>
                    <h3 style={{ padding: "10px", backgroundColor: "rgba(0,0,0,.05)" }}> Thông tin cá nhân </h3>
                    {/* Dòng tên (họ và tên) */}
                    <div className="row container-fluid" id="row1">
                        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4 w-160px" style={{ padding: "unset" }}>
                            <ul className="list-group ulInfo" >
                                <li className="list-group-item liInfo">Tên</li>
                            </ul>
                        </div>
                        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4" style={{ padding: "unset" }}>
                            <ul className="list-group ulInfo" >
                                <li className="list-group-item liInfo" id="textTenInfo" >
                                    {data.last_name + " " + data.first_name}
                                </li>
                                <li className="list-group-item liInfo" id="loaddingTen" style={{ display: "none" }}>
                                    <i className="fa fa-circle-o-notch fa-spin" ></i>
                                </li>
                            </ul>
                        </div>
                        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4" style={{ padding: "unset" }}>
                            <ul className="list-group ulInfo" >
                                <li className="list-group-item liInfo">
                                    <a data-toggle="collapse" href="#FixName" id="test" aria-expanded="false"
                                        name="fixing"
                                        onClick={this.clickTest}
                                        aria-controls="FixName">
                                        Chỉnh sửa
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    {/* div chứa thành phần collapse khi nhấn vào chỉnh sửa */}
                    <div className="collapse" id="FixName">
                        <div className="card card-body">
                            <div className="table-responsive">
                                <table className="table table-hover" style={{ margin: "unset", marginTop: "5px" }}>
                                    <tbody>
                                        <tr>
                                            <th style={{ padding: "8px 0px 0px", verticalAlign: "unset", textAlign: "center", border: "unset" }}>
                                                Họ: </th>
                                            <td style={{ padding: "unset", border: "unset" }}>
                                                <input type="text" autoComplete="off" className="form-control" name="txtHo"
                                                    id="txtHo" onKeyDown={(e) => this.handleKeyDown(e)}
                                                    // onKeyPress={(e) => this.handleKeypressTxt}
                                                    defaultValue={data.last_name}
                                                    style={{ width: "80%", float: "left" }}
                                                    placeholder="Input field" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style={{ padding: "8px 0px 0px", verticalAlign: "unset", textAlign: "center", border: "unset" }}>
                                                Tên:</th>
                                            <td style={{ padding: "unset", border: "unset" }}>
                                                <input type="text" autoComplete="off" className="form-control" name="txtTen"
                                                    id="txtTen" onKeyDown={(e) => this.handleKeyDown(e)}
                                                    // onKeyPress={(e) => this.handleKeypressTxt}
                                                    defaultValue={data.first_name}
                                                    style={{ width: "80%", float: "left" }}
                                                    placeholder="Input field" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div className="modal fade" id="ModalTen" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div className="modal-dialog" role="document">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <h5 className="modal-title" id="exampleModalLabel">Nhập mật khẩu</h5>
                                                <button type="button" className="close closeBtn" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div className="modal-body">
                                                <label>Mật khẩu:</label>
                                                <input type="password"
                                                    onKeyDown={(e) => {
                                                        if (e.key === "Enter") {
                                                            document.getElementById("submitTen").click();
                                                        }
                                                    }}
                                                    className="form-control" autoComplete="off" id="password" placeholder="Input field" />
                                            </div>
                                            <div className="modal-footer">
                                                <button type="submit" className="btn btn-primary" id="submitTen" onClick={() => this.handleSubmitTen()}
                                                >Submit</button> &nbsp;
                                                <button type="cancel" className="btn btn-default"
                                                    onClick={this.cancelTen}
                                                    data-dismiss="modal">Cancel</button>

                                                <button type="button" id="cancelTen" className="btn btn-default" style={{ display: "none" }} data-dismiss="modal">button</button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style={{ textAlign: "right", marginRight: "17.5%", padding: "5px" }}>
                                <button type="button" className="btn btn-primary"
                                    data-toggle="modal" id="luuHoTen" data-target="#ModalTen">
                                    Lưu thay đổi
                                </button> &nbsp;
                                <button className="btn btn-default" type="button" data-toggle="collapse"
                                    data-target="#FixName" aria-expanded="false" aria-controls="FixName"
                                    onClick={this.cancelTen}
                                >
                                    Hủy
                                </button>
                            </div>

                        </div>
                    </div>

                    {/* Dòng chứa tên người dùng */}
                    <div className="row container-fluid" id="row2">
                        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4 w-160px" style={{ padding: "unset" }}>
                            <ul className="list-group ulInfo" >
                                <li className="list-group-item liInfo">Tên người dùng</li>
                            </ul>
                        </div>
                        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4" style={{ padding: "unset" }}>
                            <ul className="list-group ulInfo" >
                                <li className="list-group-item liInfo">.../{data.no_sign}</li>
                            </ul>
                        </div>
                        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4" style={{ padding: "unset" }}>
                            <ul className="list-group ulInfo" >
                                <li className="list-group-item liInfo">
                                    <br />
                                </li>
                            </ul>
                        </div>
                    </div>

                    {/* dòng chứa thông tin liên hệ (sdt, email) */}
                    <div className="row container-fluid" id="row3">
                        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4 w-160px" style={{ padding: "unset" }}>
                            <ul className="list-group ulInfo" >
                                <li className="list-group-item liInfo">Liên hệ<br />(Email / số điện thoại)</li>
                            </ul>
                        </div>
                        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4" style={{ padding: "unset" }}>
                            <ul className="list-group ulInfo" >
                                <li className="list-group-item liInfo" id="loaddedContact">
                                    {data.email} <br /> {data.phone ? data.phone : "chưa có số điện thoại"}
                                </li>
                                <li className="list-group-item liInfo" id="loaddingContact" style={{ display: "none" }}>
                                    <i className="fa fa-circle-o-notch fa-spin" ></i>
                                </li>
                            </ul>
                        </div>
                        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4" style={{ padding: "unset" }}>
                            <ul className="list-group ulInfo" >
                                <li className="list-group-item liInfo">
                                    <a data-toggle="collapse" href="#FixContact"
                                        name="fixing"
                                        aria-expanded="false" onClick={this.clickTest} aria-controls="FixContact" > Chỉnh sửa<br /> </a> &nbsp;</li>
                            </ul>
                        </div>
                    </div>
                    <div className="collapse" id="FixContact">
                        <div className="card card-body">
                            <div className="row">
                                <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2" >
                                    <p style={{
                                        marginBottom: "unset", fontSize: "13px", fontWeight: "bold",
                                        padding: "8px 0 0 15px", width: "fit-content"
                                    }}> Email hiện tại:</p>
                                </div>
                                <div className="col-xs-10 col-sm-10 col-md-10 col-lg-10" style={{ paddingLeft: "unset" }}>
                                    <input type="text" autoComplete="off" className="form-control" name="txtEmail"
                                        id="txtEmail" onKeyDown={(e) => {
                                            if (e.key === "Enter")
                                                document.getElementById("luuContact").click();
                                        }}
                                        // onKeyPress={(e) => this.handleKeypressTxt}
                                        defaultValue={data.email}
                                        style={{ width: "78%", float: "left" }}
                                        placeholder="Input field" />
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                    <p style={{
                                        marginBottom: "unset", fontSize: "13px", fontWeight: "bold",
                                        padding: "8px 0 0 15px", width: "fit-content"
                                    }}> Số điện thoại:</p>
                                </div>
                                <div className="col-xs-10 col-sm-10 col-md-10 col-lg-10" style={{ paddingLeft: "unset" }}>
                                    <input type="text" autoComplete="off" className="form-control" name="txtSdt"
                                        id="txtSdt" onKeyDown={(e) => {
                                            if (e.key === "Enter")
                                                document.getElementById("luuContact").click();
                                        }}
                                        // onKeyPress={(e) => this.handleKeypressTxt}
                                        defaultValue={data.phone}
                                        style={{ width: "78%", float: "left" }}
                                        placeholder="Input field" />
                                </div>
                            </div>

                            <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div className="modal-dialog" role="document">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <h5 className="modal-title" id="exampleModalLabel">Nhập mật khẩu</h5>
                                            <button type="button" className="close closeBtn" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div className="modal-body">
                                            <label>Mật khẩu:</label>
                                            <input type="password"
                                                onKeyDown={(e) => {
                                                    if (e.key === "Enter") {
                                                        document.getElementById("luuContactbtn").click();
                                                    }
                                                }}
                                                className="form-control" autoComplete="off" id="password1" placeholder="Input field" />
                                        </div>
                                        <div className="modal-footer">
                                            <button className="btn btn-primary" data-dismiss="modal" id="luuContactbtn"
                                                onClick={() => this.handleSubmitLienHe()}
                                            >Submit</button> &nbsp;
                                                <button type="cancel" className="btn btn-default"
                                                id="cancelContact" onClick={() => {
                                                    document.getElementById("txtEmail").value = this.state.data.email;
                                                    document.getElementById("FixContact").className = "collapse"
                                                    document.getElementById("txtSdt").value = this.state.data.phone;
                                                }} data-dismiss="modal">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* </div> */}
                            <div style={{ textAlign: "right", marginRight: "17.5%", padding: "5px" }}>
                                <button type="button" className="btn btn-primary"
                                    data-toggle="modal" id="luuContact" data-target="#exampleModal">
                                    Lưu thay đổi
                                </button> &nbsp;
                                <button className="btn btn-default" type="button" data-toggle="collapse"
                                    data-target="#FixContact" aria-expanded="false" aria-controls="FixContact"
                                    onClick={() => {
                                        document.getElementById("txtEmail").value = this.state.data.email;
                                        document.getElementById("txtSdt").value = this.state.data.phone;
                                    }}
                                >
                                    Hủy
                                </button>
                            </div>
                        </div>
                    </div>

                    {/* Dòng chứa thông tin giới tính */}
                    <div className="row container-fluid" id="row4">
                        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4 w-160px" style={{ padding: "unset" }}>
                            <ul className="list-group ulInfo" >
                                <li className="list-group-item liInfo">Giới tính</li>
                            </ul>
                        </div>
                        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4" style={{ padding: "unset" }}>
                            <ul className="list-group ulInfo" >
                                <li className="list-group-item liInfo">{data.sex ? "Nam" : "Nữ"}</li>
                            </ul>
                        </div>
                        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4" style={{ padding: "unset" }}>
                            <ul className="list-group ulInfo" >
                                <li className="list-group-item liInfo"> <br /> </li>
                            </ul>
                        </div>
                    </div>
                    {/* Dòng chưa Ngày sinh */}
                    <div className="row container-fluid" id="row5">
                        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4 w-160px" style={{ padding: "unset" }}>
                            <ul className="list-group ulInfo" >
                                <li className="list-group-item liInfo">Ngày Sinh</li>
                            </ul>
                        </div>
                        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4" style={{ padding: "unset" }}>
                            <ul className="list-group ulInfo" >
                                <li className="list-group-item liInfo" id="loaddedBirth">{data.dOb}</li>
                                <li className="list-group-item liInfo" id="loaddingBirth" style={{ display: "none" }}>
                                    <i className="fa fa-circle-o-notch fa-spin" ></i>
                                </li>
                            </ul>
                        </div>
                        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4" style={{ padding: "unset" }}>
                            <ul className="list-group ulInfo" >
                                <li className="list-group-item liInfo"><a data-toggle="collapse" href="#FixNation"
                                    name="fixing"
                                    aria-expanded="false" onClick={this.clickTest} aria-controls="FixNation"> Chỉnh sửa</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className="collapse" id="FixNation">
                        <div className="card card-body">
                            <form>
                                <div className="table-responsive">
                                    <table className="table table-hover" style={{ margin: "unset", marginTop: "5px" }}>
                                        <tbody>
                                            <tr className="dateOfBirth">
                                                <th style={{ padding: "8px 0px 0px", verticalAlign: "unset", textAlign: "center", border: "unset" }}>
                                                    Ngày sinh:</th>
                                                <td style={{ padding: "unset", border: "unset" }}>
                                                    <select aria-label="Ngày" id="dayBirth"
                                                        className="inputDay  "
                                                        name="birthday_day"
                                                        title="Ngày">
                                                        <option value="0">Ngày</option>

                                                        <option value="01">01</option>

                                                        <option value="02">02</option>

                                                        <option value="03">03</option>

                                                        <option value="04">04</option>

                                                        <option value="05">05</option>

                                                        <option value="06">06</option>

                                                        <option value="07">07</option>

                                                        <option value="08">08</option>

                                                        <option value="09">09</option>

                                                        <option value="10">10</option>

                                                        <option value="11">11</option>

                                                        <option value="12">12</option>

                                                        <option value="13">13</option>

                                                        <option value="14">14</option>

                                                        <option value="15">15</option>

                                                        <option value="16">16</option>

                                                        <option value="17">17</option>

                                                        <option value="18">18</option>

                                                        <option value="19">19</option>

                                                        <option value="20">20</option>

                                                        <option value="21">21</option>

                                                        <option value="22">22</option>

                                                        <option value="23">23</option>

                                                        <option value="24">24</option>

                                                        <option value="25">25</option>

                                                        <option value="26">26</option>

                                                        <option value="27">27</option>

                                                        <option value="28">28</option>

                                                        <option value="29">29</option>

                                                        <option value="30">30</option>

                                                        <option value="31">31</option>

                                                    </select>
                                                    <select aria-label="Tháng" id="monthBirth"
                                                        className="inputDay inputThang "
                                                        name="birthday_month"
                                                        title="Tháng">
                                                        <option value="0">Tháng</option>

                                                        <option value="01">01</option>

                                                        <option value="02">02</option>

                                                        <option value="03">03</option>

                                                        <option value="04">04</option>

                                                        <option value="05">05</option>

                                                        <option value="06">06</option>

                                                        <option value="07">07</option>

                                                        <option value="08">08</option>

                                                        <option value="09">09</option>

                                                        <option value="10">10</option>

                                                        <option value="11">11</option>

                                                        <option value="12">12</option>

                                                    </select>
                                                    <select aria-label="Năm" name="birthday_year" id="yearBirth" title="Năm" className="inputDay inputThang">
                                                        <option value="0">Năm</option>
                                                        <option value="2020">2020</option>
                                                        <option value="2019">2019</option>
                                                        <option value="2018">2018</option>
                                                        <option value="2017">2017</option>
                                                        <option value="2016">2016</option>
                                                        <option value="2015">2015</option>
                                                        <option value="2014">2014</option>
                                                        <option value="2013">2013</option>
                                                        <option value="2012">2012</option>
                                                        <option value="2011">2011</option>
                                                        <option value="2010">2010</option>
                                                        <option value="2009">2009</option>
                                                        <option value="2008">2008</option>
                                                        <option value="2007">2007</option>
                                                        <option value="2006">2006</option>
                                                        <option value="2005">2005</option>
                                                        <option value="2004">2004</option>
                                                        <option value="2003">2003</option>
                                                        <option value="2002">2002</option>
                                                        <option value="2001">2001</option>
                                                        <option value="2000">2000</option>
                                                        <option value="1999">1999</option>
                                                        <option value="1998">1998</option>
                                                        <option value="1997">1997</option>
                                                        <option value="1996">1996</option>
                                                        <option value="1995">1995</option>
                                                        <option value="1994">1994</option>
                                                        <option value="1993">1993</option>
                                                        <option value="1992">1992</option>
                                                        <option value="1991">1991</option>
                                                        <option value="1990">1990</option>
                                                        <option value="1989">1989</option>
                                                        <option value="1988">1988</option>
                                                        <option value="1987">1987</option>
                                                        <option value="1986">1986</option>
                                                        <option value="1985">1985</option>
                                                        <option value="1984">1984</option>
                                                        <option value="1983">1983</option>
                                                        <option value="1982">1982</option>
                                                        <option value="1981">1981</option>
                                                        <option value="1980">1980</option>
                                                        <option value="1979">1979</option>
                                                        <option value="1978">1978</option>
                                                        <option value="1977">1977</option>
                                                        <option value="1976">1976</option>
                                                        <option value="1975">1975</option>
                                                        <option value="1974">1974</option>
                                                        <option value="1973">1973</option>
                                                        <option value="1972">1972</option>
                                                        <option value="1971">1971</option>
                                                        <option value="1970">1970</option>
                                                        <option value="1969">1969</option>
                                                        <option value="1968">1968</option>
                                                        <option value="1967">1967</option>
                                                        <option value="1966">1966</option>
                                                        <option value="1965">1965</option>
                                                        <option value="1964">1964</option>
                                                        <option value="1963">1963</option>
                                                        <option value="1962">1962</option>
                                                        <option value="1961">1961</option>
                                                        <option value="1960">1960</option>
                                                        <option value="1959">1959</option>
                                                        <option value="1958">1958</option>
                                                        <option value="1957">1957</option>
                                                        <option value="1956">1956</option>
                                                        <option value="1955">1955</option>
                                                        <option value="1954">1954</option>
                                                        <option value="1953">1953</option>
                                                        <option value="1952">1952</option>
                                                        <option value="1951">1951</option>
                                                        <option value="1950">1950</option>
                                                        <option value="1949">1949</option>
                                                        <option value="1948">1948</option>
                                                        <option value="1947">1947</option>
                                                        <option value="1946">1946</option>
                                                        <option value="1945">1945</option>
                                                        <option value="1944">1944</option>
                                                        <option value="1943">1943</option>
                                                        <option value="1942">1942</option>
                                                        <option value="1941">1941</option>
                                                        <option value="1940">1940</option>
                                                        <option value="1939">1939</option>
                                                        <option value="1938">1938</option>
                                                        <option value="1937">1937</option>
                                                        <option value="1936">1936</option>
                                                        <option value="1935">1935</option>
                                                        <option value="1934">1934</option>
                                                        <option value="1933">1933</option>
                                                        <option value="1932">1932</option>
                                                        <option value="1931">1931</option>
                                                        <option value="1930">1930</option>
                                                        <option value="1929">1929</option>
                                                        <option value="1928">1928</option>
                                                        <option value="1927">1927</option>
                                                        <option value="1926">1926</option>
                                                        <option value="1925">1925</option>
                                                        <option value="1924">1924</option>
                                                        <option value="1923">1923</option>
                                                        <option value="1922">1922</option>
                                                        <option value="1921">1921</option>
                                                        <option value="1920">1920</option>
                                                        <option value="1919">1919</option>
                                                        <option value="1918">1918</option>
                                                        <option value="1917">1917</option>
                                                        <option value="1916">1916</option>
                                                        <option value="1915">1915</option>
                                                        <option value="1914">1914</option>
                                                        <option value="1913">1913</option>
                                                        <option value="1912">1912</option>
                                                        <option value="1911">1911</option>
                                                        <option value="1910">1910</option>
                                                        <option value="1909">1909</option>
                                                        <option value="1908">1908</option>
                                                        <option value="1907">1907</option>
                                                        <option value="1906">1906</option>
                                                        <option value="1905">1905</option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div className="modal fade" id="fixBirth" tabIndex="-1" role="dialog" aria-labelledby="fixBirth" aria-hidden="true">
                                        <div className="modal-dialog" role="document">
                                            <div className="modal-content">
                                                <div className="modal-header">
                                                    <h5 className="modal-title" id="exampleModalLabel">Nhập mật khẩu</h5>
                                                    <button type="button" className="close closeBtn" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div className="modal-body">
                                                    <label>Mật khẩu:</label>
                                                    <input type="password"
                                                        className="form-control" autoComplete="off" id="password2" placeholder="Input field" />
                                                </div>
                                                <div className="modal-footer">
                                                    <button type="submit" className="btn btn-primary"
                                                        onClick={() => this.handleSubmitBirth()}
                                                        data-dismiss="modal"
                                                    >Submit</button> &nbsp;
                                                <button type="cancel" className="btn btn-default" id="cancelNation" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style={{ textAlign: "right", marginRight: "17.5%", padding: "5px" }}>
                                    <button type="button" className="btn btn-primary"
                                        data-toggle="modal" id="luuBirth" data-target="#fixBirth">
                                        Lưu thay đổi
                                </button> &nbsp;
                                <button className="btn btn-default" type="button" data-toggle="collapse"
                                        data-target="#fixBirth" aria-expanded="false" aria-controls="fixBirth"
                                        onClick={this.cancelTen}
                                    >
                                        Hủy
                                </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            )
        }
    }
    showLoadding() {
        document.getElementById("textTenInfo").style.display = "block"
        document.getElementById("loaddingTen").style.display = "none"
        document.getElementById("loaddingContact").style.display = "none"
        document.getElementById("loaddedContact").style.display = "block"
        document.getElementById("loaddedBirth").style.display = "block"
        document.getElementById("loaddingBirth").style.display = "none"
    }
    handleSubmitTen = (e) => {
        var password = "123";
        var param = {
            last_name: document.getElementById("txtHo").value,
            first_name: document.getElementById("txtTen").value,
            update_type: 0,
            password: document.getElementById("password").value
        }
        var route = "user/update/info";
        var header = {
            Authorization: 'bearer' + localStorage.getItem("UserToken")
        };
        if (document.getElementById("password").value === password || 1) {
            document.getElementById("textTenInfo").style.display = "none"
            document.getElementById("loaddingTen").style.display = "block"
            setTimeout(this.showLoadding, 5000);
            this.updataInfo(route, header, param);
            document.getElementById("FixName").className = "collapse";
            document.getElementById("password").value = '';
            document.getElementById("cancelTen").click();
            return;
        }
        if (document.getElementById("password").value === '') {
            return;
        }

    }
    myfunction() {
        document.getElementById("changeInfo").style.display = "block";
    }
    componentDidMount() {
        document.getElementById("changeInfo").disabled = true;
        document.getElementById("btnStart").click();
    }
    functionDidMount() {
        document.getElementById("changeInfo").style.display = "block"
        document.getElementById("loader").style.display = "none";

    }

    handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            document.getElementById("luuHoTen").click();
        }
    }

    cancelTen = () => {
        document.getElementById("txtHo").value = this.state.data.last_name;
        document.getElementById("txtTen").value = this.state.data.first_name;
        document.getElementById("password").value = "";
        document.getElementById("FixName").className = "collapse";
    }
    clickTest = (e) => {
        var elementClassname = document.getElementsByClassName("collapse in");
        var i;
        for (i = 0; i < elementClassname.length; i++) {
            elementClassname[i].className = "collapse";
        }

    }
    getInfo = () => {
        var params = {
            type_search: '1',
            token: localStorage.getItem("UserToken"),
        }
        var route = 'user/search-v1'
        var headers = {
            Authorization: 'bearer' + localStorage.getItem("UserToken")
        }
        var api1 = new API();
        api1.onCallAPI('get', route, {}, params, headers).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message)
            }
            else {
                console.log(res.data.data)
                this.setState({ data: res.data.data })
                this.functionDidMount();
                var dOb = res.data.data.dOb
                var date = dOb.substring(0, 2);
                var month = dOb.substring(3, 5);
                var year = dOb.substring(6);
                document.getElementById("dayBirth").value = date
                document.getElementById("monthBirth").value = month
                document.getElementById("yearBirth").value = year
            }
        });
    }
    render() {
        return (
            <div>
                <button type="button" className="btn btn-default" id="btnStart"
                    style={{ display: "none" }} onClick={() => this.getInfo()}>button</button>
                { this.onShowUserInfo()}

            </div >
        );
    }
}

export default UpdateInfoComponent;
