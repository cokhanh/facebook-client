import React, { Component } from "react";
import Messenger from "../HinhAnh/messenger.png";
import './header.css'
import API from "../API/API";
class MessengerFacebook extends Component {
  constructor(props) {
    super(props)

    this.state = {
      friendAddArray: [],
    }
  }

  componentDidMount() {
    document.getElementById("btnMessenger").click();
    var route = "chat/list-messages"
    var param = {
      token: localStorage.getItem("UserToken")
    }
    var header = {
      Authorization: "bearer" + localStorage.getItem("UserToken")
    }
    var api = new API();
    api.onCallAPI('get', route, {}, param, header).then(res => {
      if (res.data.error_code !== 0) {
        window.alert(res.data.message)
      }
      else {
        if (res.data.data) {
          this.setState({ friendAddArray: res.data.data });
        }
      }
    })
  }
  render() {
    var { friendAddArray } = this.state
    let friendAddElement = friendAddArray.map((friendAddArray, index) => {
      return (
        <li key={index} style={{ width: "100%" }}>
          <hr />

          <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2" style={{ width: "unset", padding: "unset" }}>
            <img
              src={friendAddArray.avatar}
              className="img-responsive"
              alt="Image1"
              style={{
                float: "left",
                borderRadius: "50%",
                width: "48px",
                height: "48px",
              }}
            />
          </div>


          <div className="col-xs-10 col-sm-10 col-md-10 col-lg-10" style={{ padding: "unset" }}>
            <span className="message-notification">
              {friendAddArray.friend_chat}
            </span>
            <span className="message-notification">
              {friendAddArray.messRecent}
            </span>
          </div>
        </li>
      );
    });
    return (
      <li>
        <div className="dropdown">
          <button
            className="btn btn-primary dropdown-toggle friendHeader"
            type="button"
            data-toggle="dropdown" id="btnMessenger"
          >
            <img
              src={Messenger}
              width="25px"
              height="25px"
              className="ImageProfile" style={{ marginTop: "unset" }}
              alt="facebook Profile Image1"
            />
          </button>
          <div className="dropdown-menu  dropdown-menu-right ulDropdownFriend">
            <ul>
              <li>
                <h3 style={{ fontSize: "12px", fontWeight: "600" }}>

                  Gần đây
                </h3>
              </li>
              {friendAddElement}
              <li className="liAllRequest">
                <a href="# " className="AllFriendRequest">

                  Xem tất cả
                </a>
              </li>
            </ul>
          </div>
        </div>
      </li>
    );
  }
}

export default MessengerFacebook;
