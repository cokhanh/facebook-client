import React, { Component } from "react";
// import { Route } from "react-router-dom";
import notification from '../HinhAnh/notification.png'
// import ProfilePage from "../Pages/ProfilePage";
import API from "../API/API.js";
import FriendAdd from './FriendAdd.js'
import '../Newsfeed/newsfeed.css'
import MessengerFacebook from './MessengerFacebook.js'
// import UnderHeaderProfile from "./UnderHeaderProfile.js";
class HeaderNF extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      token: "",
      arraySearch: [],
      friendRequest: [],
    };
  }

  onSearch = () => {
    var test = document.getElementById("txtTimkiem").value;
    if (!test) {
      this.setState({
        arraySearch: [],
      })
      return;
    }

    var params = {
      type_search: '0',
      search_content: test,
    }
    var route = 'user/search-v1'
    var headers = {
    }
    var { arraySearch } = this.state;
    arraySearch = [];
    var api = new API()
    api.onCallAPI('get', route, {}, params, headers).then(res => {
      if (res.data.error_code !== 0) {
        window.alert(res.data.message)
      } else {

        var userSearch = res.data.data;
        userSearch.map((user, index) => {
          var object = {
            userName: user.user_name,
            userAvatar: user.user_avatar,
            userNoSign: user.no_sign_profile
          }
          return arraySearch.push(object);
        })
        console.log(arraySearch)

        this.setState({
          arraySearch: arraySearch,
        })
      }
    }).catch(err => {
      console.log(err)
    })

  }

  onLogOut = () => {
    var route = "user/log-out";
    var header = {
      authorization: "bearer" + localStorage.getItem("UserToken")
    }
    var param = {
      api_token: localStorage.getItem("UserToken")
    }
    var api = new API()
    api.onCallAPI('post', route, {}, param, header).then(res => {
      if (res.data.error_code !== 0) {
        window.alert(res.data.message)
      } else {
        localStorage.clear();
        window.location.reload(false)

      }
    }).catch(err => {
      console.log(err)
    })


  }
  render() {
    const styleofButton = {
      width: '100%',
      textAlign: 'left',
      padding: '12px',
    }
    var { arraySearch } = this.state;
    let elementUser = arraySearch.map((user, index) => {
      return (
        <li role="row" className="liUserSearch" key={index}>
          <a href={"/" + user.userNoSign} className="aUserSearch"
            style={{ alignItems: "center", color: "#1d2129", display: "flex" }}>
            {user.userName}
          </a>
        </li >
      )
    })
    return (
      <div className="HeaderNFbar">
        <nav className="navbar navbar-inverse " style={{ border: "unset", borderRadius: "unset" }}>
          <div className="container ml-auto1">
            <div>
              <div className="col-xs-1 col-sm-1 col-md-1 col-lg-1 fbICon" >
                <a href="/">
                  <img
                    className="facebook"
                    src="https://www.facebook.com/images/fb_icon_325x325.png"
                    width="34px"
                    height="34px"
                    alt="facebook"
                    style={{ margin: "8px 0", border: "1px solid white" }}
                  />
                </a>
              </div>

              <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4 textHeader">
                <input
                  type="text"
                  id="txtTimkiem"
                  autoComplete="off"
                  className="form-control txtTimKiem"
                  onChange={() => this.onSearch()}
                  placeholder="Tìm kiếm"
                /><a href="# " onClick={() => this.onSearch()}
                  className="btn btnSearch" >
                  <span className="glyphicon glyphicon-search"></span>
                </a>

                {(this.state.arraySearch.length !== 0) && <div className="blockStoreUser">
                  <ul className="userSearch">
                    {elementUser}
                  </ul>
                </div>
                }
              </div>

              <div className="TrangChuHome">
                <ul className="nav navbar-nav" style={{ width: "unset" }}>
                  <li className="boderrightSolid">
                    <a className="mainmenubtn" href={"/" + localStorage.getItem("linkProfile")} style={styleofButton}>
                      <img
                        src={localStorage.getItem("avatar")}
                        width="24px"
                        height="24 px" style={{ marginTop: "unset" }}
                        className="ImageProfile"
                        alt="facebook Profile Image1"
                      /> &nbsp;

                       <strong className="widthUserName">{localStorage.getItem("UserName")}</strong>
                    </a>
                  </li>

                  <li className="active boderrightSolid">
                    <a className="HeaderHome" href="/">
                      <strong>Trang chủ</strong>
                    </a>
                  </li>
                  <FriendAdd />
                  <MessengerFacebook />
                  <li>
                    <div className="dropdown">
                      <button className="btn btn-primary dropdown-toggle friendHeader"
                        type="button" data-toggle="dropdown"
                      >
                        {/* #0055a8 */}
                        <img
                          src={notification}
                          width="25px"
                          height="25px"
                          className="ImageProfile" style={{ marginTop: "unset" }}
                          alt="facebook Profile Image1"
                        />
                      </button>
                      <div className="dropdown-menu  dropdown-menu-right ulDropdownFriend">
                        <ul>
                          <li>
                            <h3 style={{ fontSize: "12px", fontWeight: "600" }}> Lời mời kết bạn</h3>
                          </li>
                          {/* {friendAddElement} */}
                          <li className="liAllRequest">
                            <a href="# " className="AllFriendRequest">Xem tất cả</a>
                          </li>
                        </ul>
                      </div>

                    </div>
                  </li>

                  <li>
                    <div className="dropdown">
                      <button className="btn btn-primary dropdown-toggle friendHeader"
                        type="button" data-toggle="dropdown" style={{
                          padding: "15px 8px", background: "transparent", border: "unset"
                        }}>
                        <span className="glyphicon glyphicon-chevron-down" style={{ color: "black" }}>

                        </span>
                      </button>
                      <ul className="dropdown-menu  dropdown-menu-right">
                        <li style={{ width: "100%" }}>
                          <a role="button" className="btn btn-default"
                            style={{ padding: "6px 12px", textAlign: "left", width: "100%", border: "unset" }}
                            href="/updateinfo">
                            <i className="fas fa-wrench sidebar-icon" style={{ float: "unset", marginRight: "5px" }}></i>
                            Cài đặt</a>
                        </li>

                        <li style={{ width: "100%" }}>
                          <a role="button" className="btn btn-default"
                            style={{ padding: "6px 12px", textAlign: "left", width: "100%", border: "unset" }}
                            href="https://forms.gle/EuvfK9KHKD7a9duFA" target="_blank" rel="noopener noreferrer"
                          >
                            <i className="fas fa-tasks sidebar-icon" style={{ float: "unset", marginRight: "5px" }}></i>
                          Ý kiến đóng góp</a>
                        </li>

                        <li style={{ width: "100%", lineHeight: "unset" }}>
                          <button className="btn btn-default" style={{ width: "100%", textAlign: "left", border: "unset" }}
                            onClick={() => { this.onLogOut(); }}>
                            <i className="fas fa-sign-out-alt sidebar-icon" style={{ float: "unset", marginRight: "5px" }}></i>
                              Đăng xuất</button>
                        </li>

                        <li>

                        </li>
                      </ul>
                    </div>
                  </li>

                </ul>

              </div>

            </div>
          </div>
        </nav>
      </div >

    );
  }
}

export default HeaderNF;
