import React, { Component } from 'react';
import friendAdd from '../HinhAnh/FriendIcon.png'
import API from '../API/API.js';

class FriendAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            friendRequest: [],
            requestID: [],
        };
    }
    deleteFriendRequest = (requestID) => {
        var route = "request/delete-request";
        var param = {
            request_id: requestID
        }
        var header = {
            authorization: "bearer" + localStorage.getItem("UserToken")
        }
        var apis = new API();
        apis.onCallAPI('post', route, {}, param, header).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message)
            }
            else {
                window.location.reload(false);
            }
        }).catch(err => {
            console.log(err)
        })
    }
    acceptFriendRequest = (senderid, id1) => {
        var route = "user/update/info"
        var param = {
            friend_id: senderid,
            update_type: 3
        }
        var header = {
            authorization: "bearer" + localStorage.getItem("UserToken")
        }
        var api = new API();
        api.onCallAPI('post', route, {}, param, header).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message)
            } else {
                this.deleteFriendRequest(id1);
            }
        }).catch(err => {
            console.log(err)
        })
    }
    getfriendRequest = () => {
        var route = "request/list-request";
        var param = {
            token: localStorage.getItem("UserToken")
        }
        var header = {
            authorization: "bearer" + localStorage.getItem("UserToken")
        }
        var api = new API()
        api.onCallAPI('get', route, {}, param, header).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message)
            } else {
                var { friendRequest } = this.state
                var { requestID } = this.state
                res.data.data.map(friend => {
                    return (friendRequest.push(friend.sender), requestID.push(friend.id))
                })
                this.setState({
                    friendRequest: friendRequest,
                    requestID: requestID
                })
            }
        }).catch(err => {
            console.log(err)
        })
    }
    componentDidMount = () => {
        this.getfriendRequest();
    }
    showlistRequest = () => {
        var friendAddArray = this.state.friendRequest;
        if (friendAddArray.length!==0) {
            return friendAddArray.map((friendAddArray, index) => {
                return (
                    <li key={index} style={{ width: "100%" }}>
                        <hr />
                        <img src={friendAddArray.sender_avatar}
                            className="img-responsive" alt="Image1"
                            style={{
                                float: "left", borderRadius: "50%", width: "48px", height: "48px",
                            }} />
                        <div style={{ textAlign: "right" }}>
                            <a href={"/" + friendAddArray.no_sign_profile} className="nameFriendAdd">
                                {friendAddArray.user_name}</a>
                            <form onSubmit={(e) => e.preventDefault()}>
                                <button type="submit"
                                    onClick={() => this.acceptFriendRequest(friendAddArray.sender_id, this.state.requestID[index])}
                                    className="btn btn-info btnfriendAdd" >Xác nhận</button>
                                 &nbsp;
                                <button type="cancel" className="btn btn-default btnHuyAdd" onClick={() => this.deleteFriendRequest(this.state.requestID[index])}>Huỷ</button>
                            </form>
                        </div>
                    </li>)
            });
        }
        else {
            return (<div>
                <br/> 
                <h5 style={{textAlign:"center"}}>Không có lời mời kết bạn nào.</h5>
            </div>)
        }

    }
    render() {

        return (
            <li>
                <div className="dropdown">
                    <button className="btn btn-primary dropdown-toggle friendHeader"
                        type="button" data-toggle="dropdown"
                    >
                        <img
                            src={friendAdd}
                            width="25px"
                            height="25px"
                            className="ImageProfile" style={{marginTop:"unset"}}
                            alt="facebook Profile Image1"
                        />
                    </button>
                    <div className="dropdown-menu  dropdown-menu-right ulDropdownFriend">
                        <ul>
                            <li>
                                <h3 style={{ fontSize: "12px", fontWeight: "600" }}> Lời mời kết bạn</h3>
                            </li>
                            {this.showlistRequest()}

                        </ul>
                    </div>

                </div>
            </li>
        );
    }
}

export default FriendAdd;
