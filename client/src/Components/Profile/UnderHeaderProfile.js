import StatusContent from '../UpStatus/StatusContent.js'
import React, { Component } from 'react';
import ContentStatusNoImg from '../ContentStatus/ContentStatusNoImg.js';
import PublicImage from "../HinhAnh/Public.png";
import API from '../API/API.js';
import './profile.css'
class UnderHeaderProfile extends Component {
    constructor(props) {
        super(props)

        this.state = {
            friend: "",
            showTimeline: true,
            showFriendlist: false,
            ThemAnhBia: "",
            chinhsuaProfile: "",
            Cover: "",
            avatar: "",
            coverChanging: false,
            data: null,
            dataUser: null,
            changeAvatar: false,
            changingAvatar: false,
            frmDataAvatar: new FormData(),
            frmDataCover: new FormData(),
            friendArray: [],
            filter: null,
        }
    }
    onHandleHover = () => {
        this.setState({
            ThemAnhBia: "Thêm ảnh bìa"
        })
    }
    onHandleHoverChange = () => {
        this.setState({
            chinhsuaProfile: "Chỉnh sửa trang cá nhân"
        })
    }
    onHandleLeave = () => {
        this.setState({
            ThemAnhBia: ""
        })
    }
    onHandleLeaveChange = () => {
        this.setState({
            chinhsuaProfile: ""
        })
    }
    saveCover = () => {
        var param = {
            update_type: 2,
            type: 0,
            option: 1, //1 là cover, avartar là 0
        }
        var route = "user/update/info";
        var headers = {
            Authorization: "bearer" + localStorage.getItem("UserToken")
        }
        var api = new API();
        api.onCallAPI('post', route, this.state.frmDataCover, param, headers).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message);
                this.onHadleCancelCover();

            } else {
                localStorage.setItem("Cover", res.data.data)
                this.setState({
                    coverChanging: false
                })
                window.location.reload(false);
            }
        }).catch(err => {
            console.log(err);
        })
    }

    handleChangeFile = (event) => {
        var { frmDataCover } = this.state;
        if (event.target.files[0]) {
            frmDataCover.append("file", event.target.files[0])
            this.setState({
                Cover: URL.createObjectURL(event.target.files[0]),
                ThemAnhBia: "",
                frmDataCover: frmDataCover,
                coverChanging: true,
            })
        }
    }
    saveAvatar = () => {
        var param = {
            update_type: 2,
            type: 0,
            option: 0, //1 là cover, avartar là 0
        }
        var route = "user/update/info";
        var headers = {
            Authorization: "bearer" + localStorage.getItem("UserToken")
        }

        var api = new API();
        api.onCallAPI('post', route, this.state.frmDataAvatar, param, headers).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message);
                this.cancelAvatar();

            } else {
                localStorage.setItem("avatar", res.data.data)
                this.setState({
                    changingAvatar: false
                })
                console.log(res.data.data);
            }
        }).catch(err => {
            console.log(err);
        })
    }
    handleChangeAvatar = (e) => {
        var { frmDataAvatar } = this.state
        if (e.target.files[0]) {
            frmDataAvatar.append("file", e.target.files[0]);
            this.setState({
                avatar: URL.createObjectURL(e.target.files[0]),
                frmDataAvatar: frmDataAvatar,
                changingAvatar: true
            })
        }
    }
    cancelAvatar = () => {
        this.setState({
            avatar: "", changingAvatar: false
        });
    }
    onHadleCancelCover = () => {
        this.setState({
            Cover: "", coverChanging: false
        });
        (document.getElementById("FileBrowser1").value = null);

    }
    componentDidMount = () => {
        this.GetStatus();
        this.fillIntro();
    }

    GetStatus = () => {
        var params = {
            token: localStorage.getItem("UserToken"),
        }
        var route = 'status/load-personal-status'
        var headers = {
        }
        var api = new API()
        api.onCallAPI('get', route, {}, params, headers).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message)
            } else {
                this.setState({
                    data: res.data.data
                })
                console.log(res.data.data)
            }
        }).catch(err => {
            console.log(err)
        })
    }
    fillIntro = () => {
        const params = {
            type_search: '1',
            token: localStorage.getItem("UserToken"),
        }
        const route = 'user/search-v1'
        const headers = {
            Authorization: 'bearer' +
                localStorage.getItem("UserToken")
        }
        var api = new API()
        api.onCallAPI('get', route, {}, params, headers).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message)
            } else {
                console.log(res.data.data)
                this.setState({
                    dataUser: res.data.data,
                    friend: res.data.data.friend_array ? res.data.data.friend_array.length : "",
                    friendArray: res.data.data.friend_array
                })
                if (localStorage.getItem("avatar") !== res.data.data.avatar) {
                    localStorage.setItem("avatar", res.data.data.user_avatar);
                }
                if (localStorage.getItem("Cover") !== res.data.data.user_cover) {
                    localStorage.setItem("Cover", res.data.data.user_cover);
                }
            }
        }).catch(err => {
            console.log(err)
        })
    }
    ShowStatus = () => {
        var { data } = this.state;
        if (data) {
            return data.map((user, index) => {

                return <ContentStatusNoImg
                    notOwn="false" key={index} indexTus={index} header={user.header_content}
                    statusId={user.id} liked={user.liked} whoLike={user.who_liked_status}
                    name={user.user_name} textid={"textCmt" + user.id}
                    srcAVT={user.user_avatar} caption={user.caption} likes={user.like_number}
                    srcCaption={user.posted_image} statusSetting={user.status_setting} />
            })
        }
    }
    showDataUser = () => {
        var { dataUser } = this.state;
        if (dataUser) {

            return (
                <div>
                    <strong style={{ paddingLeft: "15px", fontSize: "1vw" }}>Email:</strong> <br />
                    <p style={{ paddingLeft: "15px", fontSize: "1vw", fontWeight: "700", color: "dodgerblue" }}> {this.state.dataUser.email}</p>
                    <strong style={{ paddingLeft: "15px", fontSize: "1vw" }}>Số điện thoại:</strong><br />
                    <p style={{ paddingLeft: "15px", fontSize: "1vw", fontWeight: "700", color: "dodgerblue" }}> {this.state.dataUser.phone}</p>
                    <strong style={{ paddingLeft: "15px", fontSize: "1vw" }}>Ngày sinh:</strong><br />
                    <p style={{ paddingLeft: "15px", fontSize: "1vw", fontWeight: "700", color: "dodgerblue" }}> {this.state.dataUser.dOb}</p>
                    <strong style={{ paddingLeft: "15px", fontSize: "1vw" }}>Giới tính:</strong><br />
                    <p style={{ paddingLeft: "15px", fontSize: "1vw", fontWeight: "700", color: "dodgerblue" }}>
                        {this.state.dataUser.sex ? "Nam" : "Nữ"}</p>
                </div>
            )
        }
    }
    filterFriend = () => {
        var { friendArray } = this.state;
        var filter = [];
        if (friendArray) {
            for (var i = 0; i < friendArray.length; i++) {
                if (friendArray[i].user_name.toLowerCase().includes(document.getElementById("friendSearch").value.toLowerCase())) {
                    filter.push(friendArray[i])
                }
            }
            this.setState({
                filter: filter
            })
        }
    }
    showFriendlist = () => {
        var { friendArray } = this.state;
        var { filter } = this.state
        if (friendArray) {
            if (filter) {
                return filter.map((friend, index) => {
                    return (
                        <div className="table-responsive">
                            <table className="table table-hover">
                                <tbody>
                                    <tr>
                                        <td>
                                            <img src={friend.avatar} width="25px" height="25px"
                                                className="img-responsive smallAvatar" alt="Image1" />
                                            <a href={"/" + friend.no_sign_profile} id={"friend" + index} className="smallUsername">
                                                {friend.user_name}
                                            </a>
                                            <button className="btn btn-defaul btnFriendList">Bạn bè</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    )
                })
            }
            return friendArray.map((friend, index) => {
                return (
                    <div key={index} className="blockFriend">
                        <img src={friend.avatar} width="25px" height="25px"
                            className="img-responsive smallAvatar" alt="Image1" />
                        <div style={{ paddingTop: "26px" }}>
                            <a href={"/" + friend.no_sign_profile} id={"friend" + index} className="smallUsername">
                                {friend.user_name}
                            </a>
                            <button className="btn btn-defaul btnFriendList">Bạn bè</button>
                        </div>
                    </div>)
            })
        }

    }
    render() {
        return (
            <div className="container mainwallProfile">
                <form onSubmit={(e) => e.preventDefault()} >
                    <div>
                        <div className="Facebook-timelineSection">
                            <div className="CoverProfile">
                                <input type="file" id="FileBrowser1" accept='image/*'
                                    style={{ display: "none" }}
                                    onChange={this.handleChangeFile}
                                />
                                {!this.state.coverChanging &&
                                    <button type="button" className="btn btn-default btnAddCover"
                                        onMouseEnter={this.onHandleHover}
                                        onMouseLeave={this.onHandleLeave}
                                        onClick={() => {
                                            document.getElementById("FileBrowser1").click();
                                        }}
                                    >
                                        {this.state.ThemAnhBia}
                                    </button>}

                                <img src={this.state.Cover ? this.state.Cover : localStorage.getItem("Cover")}
                                    className="img-responsive imgCover CoverProfile " alt="Image1" />
                                {!this.state.coverChanging &&
                                    <div className="infoProfile">
                                        <p style={{ background: "mediumpurple" }} className="NameAccount">{localStorage.getItem("UserName")}</p>
                                        <a role="button" href="/updateinfo"
                                            className="btn btn-large btn-block btn-default btnSettingINFO"
                                            onMouseEnter={this.onHandleHoverChange}
                                            onMouseLeave={this.onHandleLeaveChange}
                                        >
                                            <span className="glyphicon glyphicon-pencil" style={{ fontSize: "15px" }}></span>
                                            &nbsp;
                                            <strong style={{
                                                textDecoration: "underline"
                                            }}>{this.state.chinhsuaProfile} </strong>
                                        </a>
                                    </div>
                                }


                            </div>

                            <div className="avatarProFilePage"
                                onMouseEnter={() => this.setState({
                                    changeAvatar: true,
                                })}
                                onMouseLeave={() => this.setState({
                                    changeAvatar: false,
                                })}>
                                <img src={this.state.avatar ? this.state.avatar : localStorage.getItem("avatar")} className="img-responsive avatarProfile "
                                    alt="Image1"
                                    style={{ borderRadius: "100%" }}

                                />
                                {this.state.changeAvatar && !this.state.changingAvatar && !this.state.coverChanging && <div>
                                    <button
                                        className="btn btn-default btnChangeAva"
                                        onClick={() => {
                                            document.getElementById("fileAvatar").click();

                                        }}>
                                        <i className="fas fa-camera"></i>
                                        <br /> Cập nhật
                                    </button>
                                    <input type="file" accept='image/*' style={{ display: "none" }} id="fileAvatar" onChange={this.handleChangeAvatar} />
                                </div>}
                            </div>

                            <div className="navbar NAVinTimeline">
                                {this.state.changingAvatar && <div>
                                    <button className="btn btn-info" onClick={() => this.saveAvatar()}
                                        style={{ margin: "10px", float: "right", backgroundColor: "#2d88FF" }}>Lưu thay đổi</button>
                                    <button onClick={() => this.cancelAvatar()}
                                        className="btn btn-default" style={{ margin: "10px 0", float: "right", }}>Hủy</button>
                                    <span style={{ margin: "10px", float: "right", }} className="btnSaveCover lblCongKhaiCover">
                                        <strong>Công khai</strong>
                                    </span> </div>}

                                <ul className="nav navbar-nav ulNavTimeline" >
                                    {(!this.state.coverChanging) && <li>
                                        <a href="# " onClick={() => this.setState({
                                            showTimeline: true,
                                            showFriendlist: false,
                                        })} className="tagAinTimeLine"><strong>Dòng thời gian</strong></a>
                                    </li>}

                                    {(!this.state.coverChanging) && <li>
                                        <a href="# " id="friendProfile" role="button" className="tagAinTimeLine"
                                            onClick={() => {
                                                this.setState({
                                                    showTimeline: false,
                                                    showFriendlist: true,
                                                    filter: null
                                                });
                                            }}><strong>Bạn bè </strong> {this.state.friend ? this.state.friend : ""}</a>
                                    </li>}
                                    {(!this.state.coverChanging) && <li>
                                        <a href="# " className="tagAinTimeLine"><strong>Ảnh</strong></a>
                                    </li>}
                                    {(!this.state.coverChanging) && <li>
                                        <a href="# " className="tagAinTimeLine"><strong>Check in </strong> </a>
                                    </li>}
                                    {(!this.state.coverChanging) && <li>
                                        <a href="# " className="tagAinTimeLine tagVideoTimeline"><strong>Video</strong></a>
                                    </li>}

                                </ul>
                                {(this.state.coverChanging) &&
                                    <div className="container-fluid divBTNSaveCover">
                                        <button className="btn btnSaveCover"
                                            onClick={() => this.saveCover()}>
                                            <strong> Lưu thay đổi </strong>
                                        </button>
                                        <button
                                            onClick={() => this.onHadleCancelCover()}
                                            className="btn btnSaveCover btnHuyCover"
                                        >
                                            <strong> Huỷ </strong>
                                        </button>
                                        <span className="btnSaveCover lblCongKhaiCover">
                                            <strong>Công khai</strong>
                                        </span>
                                    </div>
                                }
                            </div>

                        </div>

                    </div>

                </form>

                <br />

                {this.state.showTimeline && <div className="row" id="DongThoiGian">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div className="row row1Intro ">
                            <div className="col-xs-1 col-sm-1 col-md-1 col-lg-1 divImageIntroHeader">
                                <img src={PublicImage} className="img-responsive imageIntro" alt="Image1" />
                            </div>
                            <p style={{ fontSize: "1.5vw", marginBottom: "unset", fontWeight: "700", color: "#3578E5" }}>
                                Giới thiệu
                             </p>
                            <br />
                            {this.showDataUser()}
                        </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                        <StatusContent avatar={this.state.dataUser ? this.state.dataUser.user_avatar : ""}
                            name={this.state.dataUser ? this.state.dataUser.user_name : ""} />
                        {/* {this.GetStatus()} */}
                        {this.ShowStatus()}
                    </div>
                </div>
                }
                {
                    this.state.showFriendlist && <div className="container-fluid" style={{ border: "1px solid black" }}>
                        <div className="row" style={{ marginTop: "10px" }}>
                            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                <h3 style={{ padding: "3px 20px" }}>Bạn bè</h3>
                            </div>
                            <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <input type="text" id="friendSearch" onChange={() => this.filterFriend()}
                                    placeholder="Tìm kiếm" className="form-control" />
                            </div>
                        </div>

                        <div>
                            <br />
                            {this.showFriendlist()}
                        </div>

                    </div>
                }

            </div >


        );
    }
}

export default UnderHeaderProfile;
