import React, { Component } from 'react';
import ContentStatusNoImg from '../ContentStatus/ContentStatusNoImg.js';
import PublicImage from "../HinhAnh/Public.png";
import './profile.css'
import API from '../API/API.js';
class StrangeFriend extends Component {
    constructor(props) {
        super(props)
        this.state = {
            Cover: "",
            avatar: "",
            data: null,
            dataUser: null,
            friend: "",
            addFriend: "https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/33EToHSZ94f.png",
            addText: "Thêm bạn bè",
            showTimeline: true,
            showFriendlist: false,
            friendArray: [],
            filter: null,
        }
    }
    deleteFriend = () => { //"huy loi moi kb"
        var route = "request/unfriend";
        var param = {
            friend_id: this.props.userId
        }
        var header = {
            Authorization: "bearer" + localStorage.getItem("UserToken")
        }
        var api = new API();
        api.onCallAPI('post', route, {}, param, header).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message)
            } else {
                this.setState(
                    {
                        addText: "Thêm bạn bè",
                        addFriend: "https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/33EToHSZ94f.png"
                    }
                )
                console.log(res.data.data)
            }
        }).catch(err => {
            console.log(err)
        })

    }
    componentDidMount = () => {
        this.GetStatus();
        this.fillIntro();
        this.checkRelations(this.props.userId);
    }
    checkRelations = (userId) => {
        if (userId) {
            var route = "user/check-relationship"
            var param = {
                friend_id: userId
            }
            var header = {
                Authorization: "bearer" +
                    localStorage.getItem("UserToken")
            }
            var api = new API()
            api.onCallAPI('get', route, {}, param, header).then(res => {
                if (res.data.error_code !== 0) {
                    window.alert(res.data.message)
                } else {
                    if (userId === this.props.userId) {
                        if (res.data.data === 0) // bạn bè 
                            this.setState(
                                {
                                    addText: "Bạn bè",
                                    addFriend: "https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/c9BbXR9AzI1.png"
                                }
                            )

                        if (res.data.data === 1) // đã gửi yêu cầu kb
                            this.setState(
                                {
                                    addText: "Hủy lời mời",
                                    addFriend: "https://static.xx.fbcdn.net/rsrc.php/v3/yr/r/aIwEkzr3XFI.png"
                                }
                            )
                        if (res.data.data === 2) {
                            document.getElementById("replyFriend").style.display = "block";
                            this.setState(
                                {
                                    addText: "Phản hồi",
                                    addFriend: "https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/c9BbXR9AzI1.png"
                                }
                            )
                        } // chờ người ta xác nhận   

                        if (res.data.data === 3) // không là bạn bè  
                            this.setState(
                                {
                                    addText: "Thêm bạn bè",
                                    addFriend: "https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/33EToHSZ94f.png"
                                }
                            )
                    }

                }
            }).catch(err => {
                console.log(err)
            })
        }

    }
    GetStatus = () => {
        var params = {
            user_id: this.props.userId,
            type_search: 1
        }
        var route = 'status/load-personal-status'
        var headers = {
        }
        var api = new API()
        api.onCallAPI('get', route, {}, params, headers).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message)
            } else {
                this.setState({
                    data: res.data.data
                })
                console.log(res.data.data)
            }
        }).catch(err => {
            console.log(err)
        })
    }
    fillIntro = () => {
        const params = {
            type_search: '1',
            user_id: this.props.userId,
        }
        const route = 'user/search-v1'
        const headers = {
            // Authorization: 'bearer' +
            //     localStorage.getItem("UserToken")
        }
        var api = new API()
        api.onCallAPI('get', route, {}, params, headers).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message)
            } else {
                this.setState({
                    dataUser: res.data.data,
                    friend: res.data.data.friend_array ? res.data.data.friend_array.length : "",
                    Cover: res.data.data.user_cover,
                    avatar: res.data.data.user_avatar,
                    friendArray: res.data.data.friend_array
                })
                console.log(res.data.data);
            }
        }).catch(err => {
            console.log(err)
        })
    }
    ShowStatus = () => {
        var { data } = this.state;
        if (data) {
            return data.map((user, index) => {

                return < ContentStatusNoImg
                    notOwn="true" key={index} indexTus={index} header={user.header_content}
                    statusId={user.id} liked={user.liked} whoLike={user.who_liked_status}
                    name={user.user_name} textid={"textCmt" + user.id}
                    srcAVT={user.user_avatar} caption={user.caption} likes={user.like_number}
                    srcCaption={user.posted_image} statusSetting={user.status_setting} />
            })
        }
    }
    showDataUser = () => {
        var { dataUser } = this.state;
        if (dataUser) {
            document.getElementById("userName").innerText = dataUser.user_name;
            return (
                <div>
                    <strong style={{ paddingLeft: "15px", fontSize: "1vw" }}>Email:</strong> <br /> <p style={{ paddingLeft: "15px", fontSize: "1vw", fontWeight: "700", color: "dodgerblue" }}> {this.state.dataUser.email}</p>
                    <strong style={{ paddingLeft: "15px", fontSize: "1vw" }}>Số điện thoại:</strong> <br /> <p style={{ paddingLeft: "15px", fontSize: "1vw", fontWeight: "700", color: "dodgerblue" }}> {this.state.dataUser.phone}</p>
                    <strong style={{ paddingLeft: "15px", fontSize: "1vw" }}>Ngày sinh:</strong> <br /> <p style={{ paddingLeft: "15px", fontSize: "1vw", fontWeight: "700", color: "dodgerblue" }}> {this.state.dataUser.dOb}</p>
                    <strong style={{ paddingLeft: "15px", fontSize: "1vw" }}>Giới tính:</strong> <br /> <p style={{ paddingLeft: "15px", fontSize: "1vw", fontWeight: "700", color: "dodgerblue" }}> {this.state.dataUser.sex ? "Nam" : "Nữ"}</p>
                </div>
            )
        }
    }
    filterFriend = () => {
        var { friendArray } = this.state;
        var filter = [];
        if (friendArray) {
            for (var i = 0; i < friendArray.length; i++) {
                if (friendArray[i].user_name.toLowerCase().includes(document.getElementById("friendSearch").value.toLowerCase())) {
                    filter.push(friendArray[i])
                }
            }
            this.setState({
                filter: filter
            })
        }
    }
    showFriendlist = () => {
        var { friendArray } = this.state;
        var { filter } = this.state;
        // var classBtn = "";
        // var txtBtn = "Thêm bạn bè";

        if (friendArray) {
            if (filter) {
                return filter.map((friend, index) => {
                    return (
                        <div className="table-responsive">
                            <table className="table table-hover">
                                <tbody>
                                    <tr>
                                        <td>
                                            <img src={friend.avatar} width="25px" height="25px"
                                                className="img-responsive smallAvatar" alt="Image1" />
                                            <a href={"/" + friend.no_sign_profile} id={"friend" + index} className="smallUsername">
                                                {friend.user_name}
                                            </a>
                                            <button className="btn btn-defaul btnFriendList">Bạn bè</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>)
                })
            }
            return friendArray.map((friend, index) => {
                return (
                    <div key={index} className="blockFriend">
                        <img src={friend.avatar} width="25px" height="25px"
                            className="img-responsive smallAvatar" alt="Image1" />
                        <div style={{ paddingTop: "26px" }}>
                            <a href={"/" + friend.no_sign_profile} id={"friend" + index} className="smallUsername">
                                {friend.user_name}
                            </a>
                            <button className="btn btn-defaul btnFriendList">Bạn bè</button>
                        </div>
                    </div>)
            })
        }

    }

    addFriend = (e) => {
        var text = e.target.innerText;
        if (text === "Thêm bạn bè") {
            document.getElementById("loaddingAdd").style.display = "block"
            var route = "request/make-friend";
            var param = {
                receiver: this.props.userId
            }
            var header = {
                Authorization: "bearer" + localStorage.getItem("UserToken")
            };
            var api = new API();
            api.onCallAPI('post', route, {}, param, header).then(res => {
                if (res.data.error_code !== 0) {
                    window.alert(res.data.message)
                } else {
                    this.setState({
                        addText: "Hủy lời mời",
                        addFriend: "https://static.xx.fbcdn.net/rsrc.php/v3/yr/r/aIwEkzr3XFI.png"
                    })
                    document.getElementById("loaddingAdd").style.display = "none"

                }
            }).catch(err => {
                console.log(err)
            })
        }
        if (text === "Bạn bè") {
            document.getElementById("btnModalUnf").click();
        }
        if (text === "Hủy lời mời") {
            var param1 = {
                friend_id: this.props.userId
            }
            var header1 = {
                Authorization: "bearer" + localStorage.getItem("UserToken")
            }
            var route1 = "request/delete-request-profile"
            var api1 = new API();
            api1.onCallAPI('post', route1, {}, param1, header1).then(res => {
                if (res.data.error_code !== 0) {
                    window.alert(res.data.message)
                } else {
                    this.setState({
                        addText: "Thêm bạn bè",
                        addFriend: "https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/33EToHSZ94f.png"
                    })
                    document.getElementById("loaddingAdd").style.display = "none"

                }
            }).catch(err => {
                console.log(err)
            })
        }
        if (text === "Phản hồi") {
            if (document.getElementById("dropdownRequest").className === "dropdown open")
                document.getElementById("dropdownRequest").className = "dropdown"
            else
                document.getElementById("dropdownRequest").className = "dropdown open"
        }
    }
    acceptRequest = () => {
        var route = "user/update/info";
        var params =
        {
            friend_id: this.props.userId,
            update_type: 3
        }
        var headers =
            { Authorization: "bearer" + localStorage.getItem("UserToken") }
        var api = new API();
        api.onCallAPI('post', route, {}, params, headers).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message)
            } else {
                document.getElementById("replyFriend").style.display = "none";
                document.getElementById("dropdownRequest").className = "dropdown"
                this.setState({
                    addText: "Bạn bè",
                    addFriend: "https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/c9BbXR9AzI1.png"
                })
                var route1 = "request/delete-request-profile"
                var param1 = {
                    friend_id: this.props.userId
                }
                var header = {
                    Authorization: "bearer" + localStorage.getItem("UserToken")
                }
                var api1 = new API();
                api1.onCallAPI('post', route1, {}, param1, header).then(res => {
                    if (res.data.error_code !== 0) {
                        window.alert(res.data.message)
                    }
                }).catch(err => {
                    console.log(err)
                })
            }
        }).catch(err => {
            console.log(err)
        })
    }
    createRoom = () => {
        var route = "chat/create-chat-group";
        var params = {
            user_id:
                this.state.dataUser.user_id,
        }
        var headers = {
            Authorization: "bearer" + localStorage.getItem("UserToken")
        }
        var api = new API();
        api.onCallAPI('post', route, {}, params, headers).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message)
            }
            else {
                if (res.data.data) {
                    console.log(res.data.data);
                }
            }
        })
    }
    render() {
        return (
            <div className="container mainwallProfile">
                <form onSubmit={(e) => e.preventDefault()} >
                    <div>
                        <div className="Facebook-timelineSection">
                            <div className="CoverProfile">
                                <img src={this.state.Cover}
                                    className="img-responsive imgCover CoverProfile " alt="Image1" />

                                <div className="infoProfile">
                                    <p style={{ background: "mediumpurple" }} id="userName" className="NameAccount">
                                        {this.state.dataUser ? this.state.dataUser.user_name : ""}
                                    </p>
                                    <div className="dropdown" id="dropdownRequest" style={{ zIndex: "10" }}>
                                        <button
                                            onClick={(e) => this.addFriend(e)}
                                            className="btn btn-large btn-block btn-default btnAddFriend"
                                        >
                                            <span>
                                                <i className="fa fa-circle-o-notch fa-spin" id="loaddingAdd" style={{ display: "none" }}></i>
                                                <img src={this.state.addFriend}
                                                    className="img-responsive addfriendImg" alt="addFriend" />
                                            </span>
                                            <strong>{this.state.addText}  </strong>
                                        </button>
                                        <button className="btn btn-default btnAddFriend btnMessenger"
                                            onClick={() => {
                                                    this.createRoom();
                                            }}
                                        >
                                            <img src="https://static.xx.fbcdn.net/rsrc.php/v3/yI/r/YIxFfN5ecJG.png"
                                                className="img-responsive addfriendImg" alt="Messenger" />
                                                Nhắn tin
                                    </button>

                                        <ul className="dropdown-menu" style={{ left: "unset", right: "12vw", top: "-3.25vh" }}>
                                            <li><a role="button" href="# " onClick={() => this.acceptRequest()}>Xác nhận</a></li>
                                            <li><a href="#  ">Xóa lời mời</a></li>
                                        </ul>
                                    </div>
                                    <button id="btnModalUnf" style={{ display: "none" }} className="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                        Launch demo modal</button>
                                    <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div className="modal-dialog" role="document">
                                            <div className="modal-content">
                                                <div className="modal-header">
                                                    <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
                                                    <button type="button" className="close closeBtn" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div className="modal-body">
                                                    Bạn có chắc chắn muốn xóa {this.state.dataUser ? this.state.dataUser.user_name : ""} khỏi danh sách bạn bè không?
                                                </div>
                                                <div className="modal-footer" style={{ borderTop: "unset" }}>
                                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Hủy</button>
                                                    <button type="button" data-dismiss="modal" onClick={() => this.deleteFriend()} className="btn btn-primary">Xác nhận</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div className="avatarProFilePage">
                                <img src={this.state.avatar} className="img-responsive avatarProfile "
                                    alt="Image1"
                                    style={{ borderRadius: "100%" }}

                                />
                            </div>

                            <div className="navbar NAVinTimeline">
                                <ul className="nav navbar-nav ulNavTimeline" >
                                    <li>
                                        <a href="# " onClick={() => this.setState({
                                            showTimeline: true,
                                            showFriendlist: false,
                                        })} className="tagAinTimeLine"><strong>Dòng thời gian</strong></a>
                                    </li>

                                    <li>
                                        <a href="# " role="button" className="tagAinTimeLine"
                                            onClick={() => {
                                                this.setState({
                                                    showTimeline: false,
                                                    showFriendlist: true,
                                                    filter: null
                                                });
                                            }}><strong>Bạn bè </strong> {this.state.friend ? this.state.friend : ""}</a>
                                    </li>

                                    <li>
                                        <a href="# " className="tagAinTimeLine"><strong>Ảnh</strong></a>
                                    </li>

                                    <li>
                                        <a href="# " className="tagAinTimeLine"><strong>Check in </strong> </a>
                                    </li>

                                    <li>
                                        <a href="# " className="tagAinTimeLine tagVideoTimeline"><strong>Video</strong></a>
                                    </li>

                                </ul>
                            </div>

                            <div style={{ margin: "10px 0", display: "none" }} id="replyFriend" >
                                <h3 style={{ fontWeight: "700", lineHeight: "1.2", float: "left", width: "fit-content", paddingTop: "10px" }}>
                                    {this.state.dataUser ? this.state.dataUser.user_name + " đã gửi cho bạn lời mời kết bạn" : ""}</h3>
                                <div style={{ textAlign: "right" }}>
                                    <button type="button" className="btn btn-lg"
                                        style={{ backgroundColor: "#1877f2", marginRight: "5px", color: "white", fontWeight: "600", padding: "6px 12px" }}>
                                        Chấp nhận lời mời</button>
                                    <button type="button" className="btn btn-lg btn-default"
                                        style={{ fontWeight: "600", padding: "6px 12px" }}
                                    >Xóa lời mời</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </form>
                {
                    this.state.showTimeline && <div className="row" style={{ marginTop: "10px" }}>
                        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div className="row row1Intro ">
                                <div className="col-xs-1 col-sm-1 col-md-1 col-lg-1 divImageIntroHeader">
                                    <img src={PublicImage} className="img-responsive imageIntro" alt="Image1" />
                                </div>
                                <p style={{ fontSize: "1.5vw", marginBottom: "unset", fontWeight: "700", color: "#3578E5" }}>
                                    Giới thiệu
                             </p>
                                <br />
                                {this.showDataUser()}
                            </div>
                        </div>
                        <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            {/* {this.GetStatus()} */}
                            {this.ShowStatus()}
                        </div>
                    </div>
                }
                <br />
                {
                    this.state.showFriendlist && <div className="container-fluid" style={{ border: "1px solid black" }}>
                        <div className="row" style={{ marginTop: "10px" }}>
                            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                <h3 style={{ padding: "3px 20px" }}>Bạn bè</h3>
                            </div>
                            <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <input type="text" id="friendSearch" onChange={() => this.filterFriend()} placeholder="Tìm kiếm" className="form-control" />
                            </div>
                        </div>

                        <div>
                            <br />
                            {this.showFriendlist()}
                        </div>

                    </div>
                }

            </div >


        );
    }
}

export default StrangeFriend;
