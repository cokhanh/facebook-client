import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
// document.addEventListener('contextmenu', event => event.preventDefault());
// document.addEventListener('keydown', (event) => {
//   if (event.keyCode === 123) // Prevent F12
//   {
//     event.preventDefault();
//   }
//   else if (event.ctrlKey && event.shiftKey && event.keyCode === 73)
//   // Prevent Ctrl+Shift+I
//   {
//     event.preventDefault();
//   }
//   else if (event.ctrlKey && event.shiftKey && event.keyCode === 67)
//   // Prevent Ctrl+Shift+C
//   {
//     event.preventDefault();
//   }
// });

ReactDOM.render(
  // <React.StrictMode>
  <App />
  // </React.StrictMode>
  ,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
