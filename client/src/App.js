import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Newsfeed from './Components/Newsfeed/Newsfeed.js';
import ProfilePage from './Pages/ProfilePage.js'
import UpdateInFo from './Pages/UpdateInFo.js'
import LoginPage from './Pages/LoginPage.js'
import lefthand from './Components/HinhAnh/lefthand.jpg'
import righthand from './Components/HinhAnh/righthand.jpg'
import StrangeFriend from './Components/Profile/StrangeFriend.js'
import API from './Components/API/API.js';
import HeaderNF from './Components/HeaderPage/HeaderNF';
import ChatComponent from './Components/Chat/srcChat/Chat/Chat.js';
import FanPages from './Pages/FanPages';
// import ChatBox from './Components/ChatBox';
// import listChat from './Components/Chat/srcChat/listChat/ListChat.js';
class App extends Component {

    constructor(props) {
        super(props)

        this.state = {
            URLproFile: localStorage.getItem("linkProfile"),
            URLproFiletest: localStorage.getItem("linkProfile"),
            url: [],
        }
    }
    componentDidMount() {
        if (localStorage.getItem("UserToken")) {
            var param = {
                api_token: localStorage.getItem("UserToken")
            }
            var header = {
                Authorization: "bearer " + localStorage.getItem("UserToken")
            }
            var route = "user/validate-log-in"
            var api = new API();
            api.onCallAPI('post', route, {}, param, header).then(res => {
                if (res.data.error_code !== 0) {
                    localStorage.clear();
                    document.getElementById("loginAtag").click();
                }
            }).catch(err => {
                console.log(err)
            })
        }
        var route1 = "user/all"
        var api1 = new API();
        api1.onCallAPI('get', route1, {}, {}, {}).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message);
            }
            else {
                this.setState({
                    url: res.data.data
                })
            }
        }).catch(err => {
            console.log(err)
        })
    }

    test = () => {
        if (localStorage.getItem("linkProfile")) {
            var { url } = this.state;
            for (var i = 0; i < url.length; i++) {
                if (url[i].no_sign_profile === localStorage.getItem("linkProfile")) {
                    break;
                }
            }
            this.state.url.splice(i, 1)
        }
        return localStorage.getItem("UserToken") && this.state.url.map((route, index) => {
            return (
                < Route path={"/" + route.no_sign_profile} key={index} exact >
                    <StrangeFriend userId={route.user_id} />
                </Route >
            )
        })
    }

    render() {
        return (
            <div>
                {/* <ChatBox /> */}
                <Router>
                    <a href="/login" id="loginAtag" style={{ display: "none" }}>ờ may dinh, gút chóp em</a>
                    {localStorage.getItem("UserToken") && <div id="pleaserotate-backdrop" >
                        <div id="pleaserotate-container">
                            <img id="leftMove" alt="Lefthand" src={lefthand} width="15%"
                                height="10%" />
                            <img id="rightMove" alt="Righthand" src={righthand} width="15%"
                                height="10%" />
                        </div>
                        <div>
                            <p id="pleaserotate-message">Chiều rộng thiết bị của
                        <br />bạn không phù hợp để bắt đầu</p>
                        </div>
                    </div>}
                    {localStorage.getItem("UserToken") && <HeaderNF />}
                    {!localStorage.getItem("UserToken") && <LoginPage />}
                    {localStorage.getItem("UserToken") && <Route path="/" exact component={Newsfeed} />}
                    {localStorage.getItem("UserToken") && <Route path={"/" + this.state.URLproFiletest} exact>
                        <ProfilePage friend={false} />
                    </Route>}
                    {localStorage.getItem("UserToken") && <Route path={"/" + this.state.URLproFiletest + "/friend"} exact>
                        <ProfilePage friend={true} />
                    </Route>}
                    {this.test()}
                    {localStorage.getItem("UserToken") && <Route path="/chat" component={ChatComponent} />}
                    {localStorage.getItem("UserToken") && <Route path="/pages" component={FanPages} />}

                    {localStorage.getItem("UserToken") && <Route path="/UpdateInfo" component={UpdateInFo} />}
                </Router>
            </div>

        );
    }
}

export default App;
