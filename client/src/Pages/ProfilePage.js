import React, { Component } from 'react';
import UnderHeaderProfile from '../Components/Profile/UnderHeaderProfile.js'
class ProfilePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            is_visible: false,
        };
    }
    componentDidMount() {
        var scrollComponent = this;
        document.addEventListener("scroll", function (e) {
            scrollComponent.toggleVisibility();
        });
    }

    toggleVisibility() {
        if (window.pageYOffset > 300) {
            this.setState({
                is_visible: true
            });
        } else {
            this.setState({
                is_visible: false
            });
        }
    }
    topFunction() {
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    }
    render() {

        return (
            <div>
                {this.state.is_visible && <button className="btn btn-default" onClick={() => this.topFunction()}
                    id="btnScrollUp" title="Go to top">
                    <span className="glyphicon glyphicon-triangle-top"></span>
                </button>}
                <UnderHeaderProfile friend={this.props.friend} strange={this.props.strange} />
            </div>
        );
    }
}

export default ProfilePage;
